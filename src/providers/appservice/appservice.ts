import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { BatchCourseData } from 'src/interfaces/BatchCourseData';
import { Api } from 'src/shared/api/api';
import { Global } from 'src/shared/global';

@Injectable()
export class AppserviceProvider {
baseURL: string = Global.BASE_API_URL;
  constructor(public http: HttpClient,private alertController:AlertController,private api:Api) {
    //this.baseURL = 'http://54.251.177.132:4500/';
  }
data:any;
  async showAlert(titledata:string,subTitledata:string) {
    const alert = await this.alertController.create({
      header: titledata,
      subHeader: subTitledata,
      buttons: ['OK']
    });
    alert.present();
  }
  checkEmployeelogin(data:any){
    debugger;
    this.data
    let body = new FormData();
body.append('Username', data.Username);
body.append('Password', data.Password);
body.append('OrgName', data.OrgName);
    var reqheaders = new HttpHeaders();
    // reqheaders.set('Content-Type','application/x-www-form-urlencoded');
     reqheaders.set('Access-Control-Allow-Origin','*');
     reqheaders.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    return this.http.post(this.baseURL+'User/EmployeeLogin', body,{headers: reqheaders});
  }

  checkParentslogin(data:any){
    debugger;
    this.data
    let body = new FormData();
body.append('Username', data.Username);
body.append('Password', data.Password);
    var reqheaders = new HttpHeaders();
    // reqheaders.set('Content-Type','application/x-www-form-urlencoded');
     reqheaders.set('Access-Control-Allow-Origin','*');
     reqheaders.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    return this.http.post(this.baseURL+'User/Login', body,{headers: reqheaders});
  }
  getStudentFeeDetails(studentId: string){
    debugger;
    return this.api.get('FeeDetail/getStudentFeeDetails?studentId=' + studentId);
  }
  getReportBaseDetails(StudentId: string,OrgId: string){
    debugger;
    return this.api.get('Report/getReportBaseDetails?StudentId='+ StudentId + '&OrgId=' + OrgId);
  }
  GetRouteCode(OrgId: string) {
    return this.api.get('GeoLocation/GetOrgBasedRouteCode?OrgId=' + OrgId);
  }
  UpdateRouteLocation(location: any) {
    return this.api.post('GeoLocation/UpdateSelectedRouteLocation', location);
  }
getEmployeeHolidays(OrgId: string){
  //debugger;
  return this.api.get('Holiday/GetEmployeeHolidaysNew?OrgId='+OrgId);
}
getPersonaldetails(EmpId: any,OrgId : any){
  //debugger;
let url = this.baseURL+'PersonalDetail/GetEmployeeDetail?EmployeeId='+EmpId+'&OrgId='+OrgId;
  let Userdetails = JSON.parse(localStorage.getItem('EmployeeLoggedData')?.toString()??"");
  const authToken =  Userdetails.token;
  const theaders = new HttpHeaders().set('Token', authToken);
  return this.http.get(url,{headers:theaders});
}

getPersonalImage(EmpId: any){
  return this.api.get('PersonalDetail/getEmployeeImage?EmpId='+EmpId);
}

getLeadDetails(OrgId: string) {
  return this.api.get('LeadMgt/getLeadDetails?OrgId=' + OrgId);
}


getBatchCourse(OrgId: string) {
  return this.http.get<BatchCourseData>('Attandance/getBatchAndCourse?OrgId='+ OrgId);
}

SaveDetails(StudentDetails: string | Blob, AcadamicDetails: string | Blob, LeadDetails: string | Blob, OrgId: string | Blob, EmpId: string | Blob) {
  let body = new FormData();
  body.append('student', StudentDetails);
  body.append('registration', AcadamicDetails);
  body.append('LeadFollowUp', LeadDetails);
  body.append('OrgId', OrgId);
  body.append('EmployeeId', EmpId);
debugger;
  return this.api.post('LeadMgt/AddNewLead', body);
}
SaveEnquiry(SaveObject: any)
{
  return this.api.post('LeadMgt/SaveEnquiry',SaveObject);
}
getStudent(studentDet: any) {
  return this.api.post('Attandance/studBasedFilter', studentDet);
}
SubmittingAttendance(AttendanceData: any) {
  return this.api.post('Attandance/SaveStudAttendanceDaily', AttendanceData);
}
getTransportStudentsBasedOnFiler(TransportStudentObj: any) {
  return this.api.post('Attandance/getTransportStudentsBasedOnFilter',TransportStudentObj);
}
SaveTransportManualAttendance(SubmittingAttendanceObject: any) {
  return this.api.post('Attandance/SaveStudTransportAttendanceManual',SubmittingAttendanceObject);
}
getRole(OrgId: string) {
  debugger;
  return this.api.get('EmployeeAttendance/getRoloes?OrgId=' + OrgId);
}
getEmployeesOnRole(EmpObj: { AttendanceDate: any; OrgId: any; RoleId: any; }) {
  debugger;
   return this.api.post('EmployeeAttendance/getEmpsOnRole', EmpObj);
}
EmpManualAttendance(EmpAttendanceManual: any) {
  return this.api.post('EmployeeAttendance/EmpManualAttendance', EmpAttendanceManual);
}

GetDaycareStudentsAttendance(DayCareObject: any) {
  return this.api.post('Attandance/GetDaycareStudentsAttendance', DayCareObject);
}

  getLatestVersion(){
    return this.api.get('appmanager/checkversion');
  }

    getStudentListBasedOnFiler(studentDet: any) {
      debugger;
      return this.api.post('Attandance/StudBasedFilter', studentDet );
    }
    submitDailyReport(SaveDairyObject: { AttendaceObj: any; dateAttendance: any; OrgId: any; Comments: any; BatchId: any; CourseId: any; Title: any; }){
      debugger;
      return this.api.post('Diary/StudDiary', SaveDairyObject);
    }
    AddMedicine(medicine: any){
      debugger;
return this.api.post('Medicine/addMedicine',medicine);
    }
    getStudentList(studentObj: any){
      debugger;
      return this.api.post('Medicine/getStudentList',studentObj);
  }
  getMedList(OrgId: string){
    return this.api.get('Medicine/getMedicineType?OrgId='+OrgId);
     }
     saveStudMed(studmed: any){
       return this.api.post('Medicine/SaveStudMedic',studmed);
     }
     addNewact(actType: any){
       debugger;
return this.api.post('StudentActivity/SaveActivityTypes',actType);
     }

     GetActTypes(OrgId: string){
       debugger;
       return this.api.get('StudentActivity/getActivityType?OrgId='+OrgId)
     }
     saveStudentActivity(report: any){
       debugger;
return this.api.post('StudentActivity/SaveStudentActivities',report);
     }
     getEmployeeslist(OrgId: string){
       debugger;
       return this.api.get('EmpAttandance/getEmployeesList?OrgId='+OrgId);
     }
     sendemployeeatt(empBCAttendance: any){
      return this.api.post('EmpAttandance/SaveEmployeeAttendanceNew',empBCAttendance);
    }
     GetStudentList(OrgId: string){
       return this.api.get('Attandance/getStudentsList?OrgId='+OrgId);
     }
     sendStudentAttnd(stdBCAttendanceData: any){
      return this.api.post('Attandance/SaveAttendanceNew',stdBCAttendanceData)
    }
     getRoutecode(OrgId: string){
       return this.api.get('GeoLocation/GetRouteCode?OrgId='+OrgId);
     }

     submitTransportAttendance(TransportattendanceData: any){
      return this.api.post('Attandance/SaveTransportNew',TransportattendanceData)
    }
     getDairyReports(Data:any)
     {
       debugger;
       return this.api.post('Diary/GetDiaryReportNew',Data);
     }
     getParentGalleryImages(Data:any)
     {
       return this.api.post('Gallery/ImageListNew',Data);
     }
     getImgDwnld(Data:any)
     {
       return this.api.get('Gallery/DownloadImage/'+Data);
     }
     ForgetPasswordParent(p: string){
      debugger;
     return this.api.get('ForgetPassword/getPassword?Email='+p);
    }
    ParentSignUp(s: string){
      debugger;
      return this.api.get('ParentRegistration/SendEmailVerificationCode?Email='+s)
    }

    ForgotPasswordEmployee(e: any){
      debugger;
      return this.api.post('ForgetPassword/getEmployeePasswordNew',e);
    }

    getStudentHolidays(OrgId: string){
      debugger;
      return this.api.get('Holiday/GetAllNew?OrgId='+OrgId);
    }

    getParentMessage(Obj: any){
      debugger;
      return this.api.post("MessageBox/getStudentMessageNew",Obj);
    }

    getStudentPersonalDetails(o: any){
      debugger;
      return this.api.post("PersonalDetail/GetAllDetailNew",o);
    }

    GetLocation(obj: any){
      debugger;
      return this.api.post('GeoLocation/ShowLocationNew',obj);
    }
}

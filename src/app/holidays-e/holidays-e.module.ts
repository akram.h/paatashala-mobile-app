import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HolidaysEPageRoutingModule } from './holidays-e-routing.module';

import { HolidaysEPage } from './holidays-e.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HolidaysEPageRoutingModule
  ],
  declarations: [HolidaysEPage]
})
export class HolidaysEPageModule {}

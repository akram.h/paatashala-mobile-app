import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HolidaysEPage } from './holidays-e.page';

describe('HolidaysEPage', () => {
  let component: HolidaysEPage;
  let fixture: ComponentFixture<HolidaysEPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(HolidaysEPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

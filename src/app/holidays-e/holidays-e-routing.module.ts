import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HolidaysEPage } from './holidays-e.page';

const routes: Routes = [
  {
    path: '',
    component: HolidaysEPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HolidaysEPageRoutingModule {}

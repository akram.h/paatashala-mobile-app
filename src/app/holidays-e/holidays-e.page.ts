import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-holidays-e',
  templateUrl: './holidays-e.page.html',
  styleUrls: ['./holidays-e.page.scss'],
})
export class HolidaysEPage implements OnInit {EmployeeLoggedData:any;
  Holidaydetail:HolidayList[];
  DBHolidayDetail: any;
   tempdata:any;
   monthNames:any;
   employee:any = null;
   constructor(public navCtrl: NavController, private AppserviceProvider:AppserviceProvider,private activatedRoute: ActivatedRoute) {
    const employeeLoggedDataString = localStorage.getItem("EmployeeLoggedData");
    this.EmployeeLoggedData = employeeLoggedDataString !== null ? JSON.parse(employeeLoggedDataString) : null;
     this.monthNames= ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
     this.DBHolidayDetail = [];
     this.Holidaydetail=[];
   }
   async ngOnInit() {
    try {
      // Fetch the employee data here (e.g., through a service or HTTP request)
      // Assign the fetched data to the 'employee' variable
      this.employee = await this.fetchEmployeeData();
      this.getEmployeeHolidays();
    } catch (error) {
      console.error('Error fetching employee data:', error);
    }
  }
  async fetchEmployeeData() {
    // Simulated fetch operation, replace with your actual implementation
    return { OrgId: 123, /* other properties */ };
  }

   ionViewDidLoad() {
     console.log('ionViewDidLoad HolidaysEPage');
   }
 getEmployeeHolidays(){
  if (this.employee && this.employee.OrgId){
    
   //debugger;
 this.AppserviceProvider.getEmployeeHolidays(this.EmployeeLoggedData.OrgId).subscribe(data =>{
  //debugger;
  this.DBHolidayDetail = data;
this.DBHolidayDetail.forEach((element: any) => {
let month = this.Holidaydetail.find(res=>res.MonthName==element.MonthName);
if(month==null||month==undefined)
{
  let newMonth = new HolidayList();
  newMonth.MonthName = element.MonthName;
  let holiday = new HolidayObj();
  holiday.Date = element.Date;
  holiday.Name = element.HolidayName;
  newMonth.HolidayObjList = [];
  newMonth.HolidayObjList.push(holiday);
  this.Holidaydetail.push(newMonth);
}else{
  let holiday = new HolidayObj();
  holiday.Date = element.Date;
  holiday.Name = element.HolidayName;
  month.HolidayObjList.push(holiday)
}
});
// this.tempdata = data;
// console.log(this.tempdata);
// this.noHolidays = this.tempdata.length == 0;
// for (let i = 0; i < this.tempdata.Holidays.length; i++) {
//   let tem= new Date(this.tempdata.Holidays[i].Date);
//   let tempMonthName = this.monthNames[tem.getMonth()];
// this.j++;
//   let tempMonthIndex = tem.getMonth();
//   if (!this.HolidayDetail[tempMonthIndex]) {
//     this.HolidayDetail[tempMonthIndex] = new HolidayList();
//     this.HolidayDetail[tempMonthIndex].Index = tempMonthIndex;
//     this.HolidayDetail[tempMonthIndex].MonthName = tempMonthName;
//     // = {
//     //       Index: tempMonthIndex,
//     //       MonthName: tempMonthName,
//     //   };
//   }
//   this.HolidayDetail[tempMonthIndex].List.push({
//       Date: new Date(this.tempdata.Holidays[i].Date),
//       Name: this.tempdata.Holidays[i].Name.trim()
//   });
// this.HolidayDetail
// }
// console.log(this.j);
// console.log(this.HolidayDetail[1]);
})





  }
 }
}
class HolidayList
{

  MonthName : string ="";
  HolidayObjList!: HolidayObj[];
 
}
class HolidayObj{
  Date : any;
  Name! : string;
}

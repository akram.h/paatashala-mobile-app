import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
@Component({
  selector: 'app-employee-forgotpassword',
  templateUrl: './employee-forgotpassword.page.html',
  styleUrls: ['./employee-forgotpassword.page.scss'],
})
export class EmployeeForgotpasswordPage implements OnInit {
  EmpCred:any;
  Result:any;
    constructor(public navCtrl: NavController,  private appserviceProvider:AppserviceProvider) {
      this.EmpCred = {
        Email:'',
        OrgName:''
      };
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad EmployeeForgetpasswordPage');
    }
    ForgotPassword(){
      debugger;
      this.appserviceProvider.ForgotPasswordEmployee(this.EmpCred).subscribe(
        data=>{
          debugger;
          console.log(data);
          this.Result=data;
          if(this.Result.status==true){
            this.appserviceProvider.showAlert("Success",this.Result.Message);
          }else{
            this.appserviceProvider.showAlert("Error",this.Result.Message);
          }
        }
      );
    }


  ngOnInit() {
  }

}

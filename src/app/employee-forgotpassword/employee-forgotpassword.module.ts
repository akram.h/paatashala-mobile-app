import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';

import { EmployeeForgotpasswordPageRoutingModule } from './employee-forgotpassword-routing.module';

import { EmployeeForgotpasswordPage } from './employee-forgotpassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmployeeForgotpasswordPageRoutingModule
  ],
  declarations: [EmployeeForgotpasswordPage],
  providers:[AppserviceProvider],
})
export class EmployeeForgotpasswordPageModule {}

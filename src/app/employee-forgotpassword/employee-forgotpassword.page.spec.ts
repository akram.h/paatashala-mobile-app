import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EmployeeForgotpasswordPage } from './employee-forgotpassword.page';

describe('EmployeeForgotpasswordPage', () => {
  let component: EmployeeForgotpasswordPage;
  let fixture: ComponentFixture<EmployeeForgotpasswordPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EmployeeForgotpasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

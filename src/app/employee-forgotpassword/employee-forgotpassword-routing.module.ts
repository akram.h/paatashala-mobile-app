import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeForgotpasswordPage } from './employee-forgotpassword.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeeForgotpasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeForgotpasswordPageRoutingModule {}

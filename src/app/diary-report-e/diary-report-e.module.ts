import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';

import { DiaryReportEPageRoutingModule } from './diary-report-e-routing.module';

import { DiaryReportEPage } from './diary-report-e.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiaryReportEPageRoutingModule
  ],
  declarations: [DiaryReportEPage],
  providers: [AppserviceProvider],


})
export class DiaryReportEPageModule {}

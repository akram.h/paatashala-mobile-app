import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DiaryReportEPage } from './diary-report-e.page';

describe('DiaryReportEPage', () => {
  let component: DiaryReportEPage;
  let fixture: ComponentFixture<DiaryReportEPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DiaryReportEPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

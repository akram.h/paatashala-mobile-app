import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiaryReportEPage } from './diary-report-e.page';

const routes: Routes = [
  {
    path: '',
    component: DiaryReportEPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiaryReportEPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';

@Component({
  selector: 'app-diary-report-e',
  templateUrl: './diary-report-e.page.html',
  styleUrls: ['./diary-report-e.page.scss'],
})
export class DiaryReportEPage implements OnInit {

  batchList: any;
  BatchObject: any;
  EmployeeLoggedData: any;
  courseList: any;
  DairyReportObject: any;
  studentList: any;
  submittedValue: any;
  DiaryReportStudent: any;
  Onmark: any;
  Result: any;
  tempList: any;

  constructor(public navCtrl: NavController, private appserviceprovider: AppserviceProvider) {
    this.Onmark = [];
    this.tempList = [];
    this.batchList = [];
    this.BatchObject = {};
    this.courseList = [];
    this.studentList = [];
    this.DairyReportObject = {};
    this.DairyReportObject.AttendanceDate = new Date().toISOString();
    this.DiaryReportStudent = {
      markAll: '',
      Title: '',
      Note: ''
    };
    this.submittedValue = true;
    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") as string);

  }

  ngOnInit() {
    this.getBatchandCourse();
    
    this.Onmark = [
      { Id: true, Name: 'All' },
      { Id: false, Name: 'None' }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiaryReportEPage');
  }

  // getBatchandCourse() {
  //   this.appserviceprovider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
  //     data => {
  //       this.BatchObject = data;
  //       this.batchList = this.BatchObject.Batches;
  //       this.courseList = this.BatchObject.Courses;
  //     }
  //   );
  // }
  getBatchandCourse() {
    if (this.EmployeeLoggedData && this.EmployeeLoggedData.OrgId) {
      this.appserviceprovider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
        data => {
          this.BatchObject = data;
          this.batchList = this.BatchObject.Batches;
          this.courseList = this.BatchObject.Courses;
        },
        error => {
          // Handle the error if necessary
        }
      );
    } else {
      // Handle the case when OrgId is not available in EmployeeLoggedData
    }
  }
  
  getStudentListBasedOnFilter() {
    this.submittedValue = false;
    this.DairyReportObject.OrgId = this.EmployeeLoggedData.OrgId;
    this.appserviceprovider.getStudentListBasedOnFiler(this.DairyReportObject).subscribe(
      data => {
        this.studentList = data;
        this.tempList = data;
      }
    );
  }

  SelectAll() {
    console.log(this.DiaryReportStudent);
    this.studentList.forEach((element: any) => {
      element.isPresent = this.DiaryReportStudent.markAll;
    });
  }

  submtDiaryReport() {
    let SaveDairyObject = {
      AttendaceObj: this.studentList,
      dateAttendance: this.DairyReportObject.AttendanceDate.toString(),
      OrgId: this.EmployeeLoggedData.OrgId,
      Comments: this.DiaryReportStudent.Note,
      BatchId: this.DairyReportObject.BatchId,
      CourseId: this.DairyReportObject.CourseId,
      Title: this.DiaryReportStudent.Title
    };

    this.appserviceprovider.submitDailyReport(SaveDairyObject).subscribe(
      data => {
        this.Result = data;
        if (this.Result.status == true) {
          this.appserviceprovider.showAlert('Saved Successfully', '');
          this.studentList.forEach((element:any) => {
            element.isPresent = false;
          });
          this.DiaryReportStudent = {
            markAll: '',
            Title: '',
            Note: ''
          };
        } else {
          this.appserviceprovider.showAlert('Failed', this.Result.message);
        }
      });
  }

  onInput(event: any) {
    this.studentList = this.studentList.filter((item: any) => {
      return (item.StudentName.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1);
    });
    

  }
}

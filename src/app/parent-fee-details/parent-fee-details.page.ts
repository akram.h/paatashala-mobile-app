import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-parent-fee-details',
  templateUrl: './parent-fee-details.page.html',
  styleUrls: ['./parent-fee-details.page.scss'],
})
export class ParentFeeDetailsPage implements OnInit {
  StudentFeeDetails: any;
  studentData: any;
    constructor(public navCtrl: NavController, private appserviceProvider:AppserviceProvider) {
      this.StudentFeeDetails = [];
      this.studentData = {};
      this.studentData = JSON.parse(localStorage.getItem("CurrentStudent")!);
    }
    ionViewDidLoad() {
      console.log('ionViewDidLoad ParentFeeDetailsPage');
      this.getStudentFeeDetails();
    }
  
    getStudentFeeDetails() {
      this.appserviceProvider.getStudentFeeDetails(this.studentData.StudentId).subscribe(
        data => {
          debugger;
          this.StudentFeeDetails = data;
        },
      )
    }
  
  
  ngOnInit() {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentFeeDetailsPage } from './parent-fee-details.page';

describe('ParentFeeDetailsPage', () => {
  let component: ParentFeeDetailsPage;
  let fixture: ComponentFixture<ParentFeeDetailsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentFeeDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

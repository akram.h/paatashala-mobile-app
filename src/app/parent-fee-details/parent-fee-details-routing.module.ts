import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentFeeDetailsPage } from './parent-fee-details.page';

const routes: Routes = [
  {
    path: '',
    component: ParentFeeDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentFeeDetailsPageRoutingModule {}

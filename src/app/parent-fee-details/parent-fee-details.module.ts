import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentFeeDetailsPageRoutingModule } from './parent-fee-details-routing.module';

import { ParentFeeDetailsPage } from './parent-fee-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentFeeDetailsPageRoutingModule
  ],
  declarations: [ParentFeeDetailsPage]
})
export class ParentFeeDetailsPageModule {}

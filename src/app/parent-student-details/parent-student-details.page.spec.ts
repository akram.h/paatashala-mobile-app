import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentStudentDetailsPage } from './parent-student-details.page';

describe('ParentStudentDetailsPage', () => {
  let component: ParentStudentDetailsPage;
  let fixture: ComponentFixture<ParentStudentDetailsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentStudentDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

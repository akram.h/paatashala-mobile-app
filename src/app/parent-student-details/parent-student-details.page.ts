import { Component, OnInit } from '@angular/core';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { NavController } from '@ionic/angular';
import { Global } from 'src/shared/global';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-parent-student-details',
  templateUrl: './parent-student-details.page.html',
  styleUrls: ['./parent-student-details.page.scss'],
})
export class ParentStudentDetailsPage implements OnInit {
  url: string = Global.BASE_API_URL;
SDetails:any;
SelectedStudentData:any;
test: any;
x:any;
 imgdata:any;
  base64Data: any;
  converted_image: any;
  constructor(public navCtrl: NavController,private activatedRoute: ActivatedRoute,private suneel:AppserviceProvider) {
    this.SelectedStudentData = JSON.parse(localStorage.getItem("CurrentStudent") || '{}');
    this.SDetails={};
    this.x ={
      StudentId:'',
      OrgId:''
    };
  }
ngOnInit() {
  const studentId = this.activatedRoute.snapshot.paramMap.get('studentId');
 this.getStudentDetails();
this.imgdata =  this.url+'Student/StudentImage?Id='+this.SelectedStudentData.StudentId;
}

getStudentDetails(){
  debugger;

this.x.StudentId = this.SelectedStudentData.StudentId;
this.x.OrgId = this.SelectedStudentData.OrgId;
this.suneel.getStudentPersonalDetails(this.x).subscribe(
  data=>{
    debugger;
    this.SDetails=data;
    console.log(this.SDetails);
    debugger;
  }
);
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentStudentDetailsPage');
  }


 

}

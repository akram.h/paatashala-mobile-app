import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentStudentDetailsPageRoutingModule } from './parent-student-details-routing.module';

import { ParentStudentDetailsPage } from './parent-student-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentStudentDetailsPageRoutingModule
  ],
  declarations: [ParentStudentDetailsPage]
})
export class ParentStudentDetailsPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentStudentDetailsPage } from './parent-student-details.page';

const routes: Routes = [
  {
    path: '',
    component: ParentStudentDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentStudentDetailsPageRoutingModule {}

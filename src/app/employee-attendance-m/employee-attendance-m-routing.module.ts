import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeAttendanceMPage } from './employee-attendance-m.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeeAttendanceMPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeAttendanceMPageRoutingModule {}

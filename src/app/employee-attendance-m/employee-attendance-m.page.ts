import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
@Component({
  selector: 'app-employee-attendance-m',
  templateUrl: './employee-attendance-m.page.html',
  styleUrls: ['./employee-attendance-m.page.scss'],
})
export class EmployeeAttendanceMPage implements OnInit {
  EmpAttendanceObject: any;
  RoleList: any;
  EmployeeLoggedData: any;
  NextPage: any;
  EmployeeList: any;
  AttendanceMarkAll: any;
  SearchEmployee: any;
  NextPageDiv: boolean;
  EmpAttendanceManualObject: any;
  
    constructor(public navCtrl: NavController,  private appserviceProvider:AppserviceProvider) {
      this.EmpAttendanceObject = {};
      this.EmployeeLoggedData = {};
      this.NextPage = {};
      this.EmpAttendanceManualObject = {};
      this.RoleList = [];
      this.EmployeeList = [];
      this.SearchEmployee = [];
      this.AttendanceMarkAll = [
        { Id: true, Name: 'Present'},
        { Id: false, Name: 'Absent'}
      ];
      this.NextPageDiv = false;
  
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad EmployeeAttendanceMPage');
      // this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData"));
      this.EmployeeLoggedData = JSON.parse(localStorage.getItem('EmployeeLoggedData') as any);
      this.getRole();
    }
  
    getRole() {
      debugger;
    this.appserviceProvider.getRole(this.EmployeeLoggedData.OrgId).subscribe(
    data => {
        this.RoleList = data;
        console.log(data);
    }
  );
    }
    getEmployee() {
      let EmpObj = {
        AttendanceDate: this.EmpAttendanceObject.AttendanceDate.toString(),
        OrgId: this.EmployeeLoggedData.OrgId,
        RoleId: this.EmpAttendanceObject.RoleId
      };
      this.appserviceProvider.getEmployeesOnRole(EmpObj).subscribe(
        data => {
          this.NextPageDiv = true;
          this.EmployeeList = data;
          this.SearchEmployee = this.EmployeeList;
          debugger;
        }
      );
    }
  
    MarkAllStud() {
      debugger;
      this.EmployeeList.forEach((element: any) => {
        element.isPresent = this.NextPage.Mark;
      });
      debugger;
    }
  
    SearchEmployeeName(Event: any) {
      debugger;
      this.EmployeeList = this.SearchEmployee;
      const val = Event.target.value;
      if (val && val.trim() != '') {
        this.EmployeeList = this.EmployeeList.filter((item: any) => {
          return (item.Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
    onCancel(){}
  
    SaveAttendance() {
      this.EmpAttendanceManualObject.AttendaceObj = this.EmployeeList;
      this.EmpAttendanceManualObject.dateAttendance = this.EmpAttendanceObject.AttendanceDate;
      this.EmpAttendanceManualObject.OrgId = this.EmployeeLoggedData.OrgId;
      this.appserviceProvider.EmpManualAttendance(this.EmpAttendanceManualObject).subscribe(
        data => {
          debugger;
          this.appserviceProvider.showAlert('Saved Successfully', '');
        }
      );
    }
  
  
  ngOnInit() {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';

import { EmployeeAttendanceMPageRoutingModule } from './employee-attendance-m-routing.module';

import { EmployeeAttendanceMPage } from './employee-attendance-m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmployeeAttendanceMPageRoutingModule
  ],
  declarations: [EmployeeAttendanceMPage],
  providers: [AppserviceProvider],

})
export class EmployeeAttendanceMPageModule {}

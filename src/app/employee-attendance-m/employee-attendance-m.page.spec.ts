import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EmployeeAttendanceMPage } from './employee-attendance-m.page';

describe('EmployeeAttendanceMPage', () => {
  let component: EmployeeAttendanceMPage;
  let fixture: ComponentFixture<EmployeeAttendanceMPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EmployeeAttendanceMPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

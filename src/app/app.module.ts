import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BaseRouteReuseStrategy as RouteReuseStrategy } from '@angular/router';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { Router, Route } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Api } from 'src/shared/api/api';
import { Base } from 'src/shared/base/base';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule,HttpClientModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [AppserviceProvider,{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, Api, Base],
  bootstrap: [AppComponent],
})
export class AppModule {}

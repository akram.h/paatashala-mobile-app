import { Component,Inject,inject, OnInit } from '@angular/core';
import { NavController,NavParams } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { Base } from 'src/shared/base/base';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  version !: string;
  versionNumber: string = "0.0";
  versionObj: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, @Inject(AppVersion) private appVersion: any,  private appserviceProvider: AppserviceProvider,private base:Base) {
    this.getVersion();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  getVersion() {
    debugger;
    this.appVersion.getVersionNumber().then((res: any) =>{
      debugger;
      this.versionNumber=res;
      })
  }


  getVersionDetails() {
    this.appVersion.getVersionNumber().then((res: any) => {
      this.version = res;
      alert(this.version);
    })
  }

  checkUpdate() {
    debugger;
    this.appserviceProvider.getLatestVersion().subscribe(
      data => {
        debugger;
        this.versionObj = data;
        if (this.versionNumber >= this.versionObj.Version) {
             this.base.presentAlert("App is Up to Date");
        }
        else {
this.base.clickDownloadAlert();
        }
        (err: any) => {
          debugger;
          console.error(err);
        }
      }
    );
  }


  ngOnInit() {
  }

}

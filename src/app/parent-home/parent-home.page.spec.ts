import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentHomePage } from './parent-home.page';

describe('ParentHomePage', () => {
  let component: ParentHomePage;
  let fixture: ComponentFixture<ParentHomePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

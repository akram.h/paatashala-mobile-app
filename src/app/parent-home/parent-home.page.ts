import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParentDiaryReportPage } from '../parent-diary-report/parent-diary-report.page';
import { ParentFeeDetailsPage } from '../parent-fee-details/parent-fee-details.page';
import { ParentMessageBoxPage } from '../parent-message-box/parent-message-box.page';
import { ParentPhtogalleryPage } from '../parent-phtogallery/parent-phtogallery.page';
import { ParentReportsPage } from '../parent-reports/parent-reports.page';
import { ParentStudentDetailsPage } from '../parent-student-details/parent-student-details.page';
import { ParentTrackStudentPage } from '../parent-track-student/parent-track-student.page';
import { ParentHolidaysPage } from '../parent-holidays/parent-holidays.page';
import { Router } from '@angular/router';
@Component({
  selector: 'app-parent-home',
  templateUrl: './parent-home.page.html',
  styleUrls: ['./parent-home.page.scss'],
})
export class ParentHomePage implements OnInit {

  constructor(private navCtrl: NavController,private router: Router) {}

 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentHomePage');
  }

  GoParentDiaryReport() {
    this.navCtrl.navigateForward('/parent-diary-report');
  }

  GoParentFeeDetails() {
    this.navCtrl.navigateForward('/parent-fee-details');
  }

  GoParentHolidays() {
    this.navCtrl.navigateForward('/parent-holidays');
  }

  GoParentMessageBox() {
    this.navCtrl.navigateForward('/parent-message-box');
  }

  GoParentPhotoGallery() {
    this.navCtrl.navigateForward('/parent-photo-gallery');
  }

  GoParentReports() {
    this.navCtrl.navigateForward('/parent-reports');
  }

  GoParentStudentDetails() {
    this.navCtrl.navigateForward('/parent-student-details');
  }

  GoParentTrackStudent() {
    this.navCtrl.navigateForward('/parent-track-student');
  }


  ngOnInit() {
  }

}

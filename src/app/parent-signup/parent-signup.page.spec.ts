import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentSignupPage } from './parent-signup.page';

describe('ParentSignupPage', () => {
  let component: ParentSignupPage;
  let fixture: ComponentFixture<ParentSignupPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentSignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

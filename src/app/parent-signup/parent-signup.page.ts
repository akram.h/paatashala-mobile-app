import { Component, OnInit } from '@angular/core';
import { NavController, NavParams} from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';


@Component({
  selector: 'app-parent-signup',
  templateUrl: './parent-signup.page.html',
  styleUrls: ['./parent-signup.page.scss'],
})
export class ParentSignupPage implements OnInit {
  Email:any;
    Result: any;
    constructor(public navCtrl: NavController, public navParams: NavParams,private appServiceProvider:AppserviceProvider) {
      this.Email="";
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad ParentSignupPage');
    }
    ParentSignUp(){
  this.appServiceProvider.ParentSignUp(this.Email).subscribe(
    data=>{
      this.Result=data;
      if(this.Result.status){
  this.appServiceProvider.showAlert('Successful',"Code send to email",);
      }else{
        this.appServiceProvider.showAlert('Fail',this.Result.Message);
      }
    }
  )
    }
  

  ngOnInit() {
  }

}

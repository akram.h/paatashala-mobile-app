import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentSignupPage } from './parent-signup.page';

const routes: Routes = [
  {
    path: '',
    component: ParentSignupPage,
  }
];

@NgModule({
  imports: [
    // ...
    RouterModule.forRoot(routes),
    // ...
  ],
  exports: [RouterModule],
})
export class ParentSignupPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';
import { AppComponent } from '../app.component';
import { ParentSignupPageRoutingModule } from './parent-signup-routing.module';
import { RouterModule } from '@angular/router';
import { ParentSignupPage } from './parent-signup.page';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    IonicModule,
    ParentSignupPageRoutingModule
  ],
  declarations: [ParentSignupPage]
})
export class ParentSignupPageModule {}

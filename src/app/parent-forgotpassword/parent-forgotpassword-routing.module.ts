import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentForgotpasswordPage } from './parent-forgotpassword.page';

const routes: Routes = [
  {
    path: '',
    component: ParentForgotpasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentForgotpasswordPageRoutingModule {}

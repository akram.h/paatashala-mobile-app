import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentForgotpasswordPageRoutingModule } from './parent-forgotpassword-routing.module';

import { ParentForgotpasswordPage } from './parent-forgotpassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentForgotpasswordPageRoutingModule
  ],
  declarations: [ParentForgotpasswordPage]
})
export class ParentForgotpasswordPageModule {}

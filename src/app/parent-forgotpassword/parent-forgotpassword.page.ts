import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';

@Component({
  selector: 'app-parent-forgotpassword',
  templateUrl: './parent-forgotpassword.page.html',
  styleUrls: ['./parent-forgotpassword.page.scss'],
})
export class ParentForgotpasswordPage implements OnInit {
  Email:any;
  Result:any;
    constructor(public navCtrl: NavController,  public appServiceProvider: AppserviceProvider) {
   this.Email='';
  }

    ionViewDidLoad() {
      console.log('ionViewDidLoad ParentForgetpasswordPage');
    }
  ForgotPassword(){
  this.appServiceProvider.ForgetPasswordParent(this.Email).subscribe(
    data=>{
      debugger;
      console.log(data);
      this.Result=data;
      if(this.Result.status==true){
        this.appServiceProvider.showAlert("Success",this.Result.Message);
      }else{
        this.appServiceProvider.showAlert("Error",this.Result.Message);
      }
    }
  );}


  ngOnInit() {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentForgotpasswordPage } from './parent-forgotpassword.page';

describe('ParentForgotpasswordPage', () => {
  let component: ParentForgotpasswordPage;
  let fixture: ComponentFixture<ParentForgotpasswordPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentForgotpasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

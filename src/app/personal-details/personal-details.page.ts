import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.page.html',
  styleUrls: ['./personal-details.page.scss'],
})
export class PersonalDetailsPage implements OnInit {
  imageUrl : any;
  PersonalDetails : any;
  EmployeeLoggedData : any;
  handleImageError(event: any) {
    event.target.style.display = 'none';
  }
  constructor(public navCtrl: NavController,private activatedRoute: ActivatedRoute, private AppserviceProvider:AppserviceProvider) {
    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") || '{}');
    this.PersonalDetails = {
      Name:'',
      EmpId:'',
      Designation:''
    };
  }
  ngOnInit(){
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
    });
    this.getPersonalDetails();
   // this.getImage();
  }
  getPersonalDetails(){
    this.AppserviceProvider.getPersonaldetails(this.EmployeeLoggedData.EmpId,this.EmployeeLoggedData.OrgId).subscribe(data =>{
      debugger;
      let asd:any=data;
      this.PersonalDetails = asd.Data;
      console.log(data);
    })
  }
  getImage(){
    this.AppserviceProvider.getPersonalImage(this.EmployeeLoggedData.EmpId).subscribe(data =>{
      debugger;
      this.imageUrl = data;
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PersonalDetailsPage');
  }



}

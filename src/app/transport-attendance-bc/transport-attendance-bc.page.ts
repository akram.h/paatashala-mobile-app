import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-transport-attendance-bc',
  templateUrl: './transport-attendance-bc.page.html',
  styleUrls: ['./transport-attendance-bc.page.scss'],
})
export class TransportAttendanceBcPage implements OnInit {
  LoggedEmployeeData:any;
  StudentList: any;
  scannedStudentList:any;
  Position:any;
  Pick:any;
  TransportAttendanceData: any;
  result: any;
  m: any;
  constructor(public navCtrl: NavController,private route: ActivatedRoute,private appserviceProvider:AppserviceProvider, private barcodeScanner:BarcodeScanner ) {
    const parameterValue = this.route.snapshot.params['parameterName'];
  this.LoggedEmployeeData = JSON.parse(localStorage.getItem("EmployeeLoggedData") || '{}');

this.StudentList=[];
this.scannedStudentList = {
  Id: [],
  Name: [],
  StudentId: []
}
this.m = false;
this.TransportAttendanceData = {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransportAttendanceBcPage');
  }
  ngOnInit() {
    this.getsTudentList();
    this.Position = "School";
  }

  getsTudentList(){
    this.appserviceProvider.GetStudentList(this.LoggedEmployeeData.OrgId).subscribe(
      data=>{
        debugger;
        console.log(data);
        this.StudentList = data;
      }
    );
      }

      // getRouteCode(){
      // this.appserviceProvider.getRoutecode(this.LoggedEmployeeData.OrgId).subscribe(
      //   data=>{
      //     debugger;
      //     console.log(data);

      //   }
      // );
      // }
  ScanBar(){
    this.barcodeScanner.scan().then(
      barcodeData=>{
        let found=true;
        let Id = barcodeData.text;
        this.StudentList.forEach((element: any) => {
          if(element.StudentId==Id){
            found = false;
            this.scannedStudentList.Name.push(element.Name);
            this.scannedStudentList.Id.push(element.Id);
            this.scannedStudentList.StudentId.push(element.StudentId);
          }
        });
        // if(this.scannedStudentList.Id.length == 0){
        //   console.log("Invalid Student ID");
        //   this.appserviceProvider.showAlert('Invalid Student ID',"Please Try again");
        // }
         console.log(this.scannedStudentList);
      }
    ).catch(err => {
      console.log('Error', err);
  });
  }
  deleteRow(ind: any){
    this.scannedStudentList.Id.splice(ind,1);
    this.scannedStudentList.Name.splice(ind,1);
    this.scannedStudentList.StudentId.splice(ind,1);
  }
  submitStudTransportAttendance(){
    this.TransportAttendanceData.OrgId = this.LoggedEmployeeData.OrgId;
    this.TransportAttendanceData.StudentId = this.scannedStudentList.Id.toString();
    this.TransportAttendanceData.scanDateTime = new Date().toString();
    this.TransportAttendanceData.IsPickUp = this.Pick;
    this.TransportAttendanceData.Position = this.Position;
    debugger;
    this.appserviceProvider.submitTransportAttendance(this.TransportAttendanceData).subscribe(
      data=>{
        debugger;
     this.result=data;
     if(this.result.status==true){
       this.appserviceProvider.showAlert("Successfully saved","");
       this.scannedStudentList = {
        Id: [],
        Name: [],
        StudentId: []
      }
     }  else {
       this.appserviceProvider.showAlert("Failed",this.result.message);
     }
      }
    );
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { IonicModule } from '@ionic/angular';

import { TransportAttendanceBcPageRoutingModule } from './transport-attendance-bc-routing.module';

import { TransportAttendanceBcPage } from './transport-attendance-bc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransportAttendanceBcPageRoutingModule
  ],
  providers: [
    BarcodeScanner,
    
  ],
  declarations: [TransportAttendanceBcPage]
})
export class TransportAttendanceBcPageModule {}

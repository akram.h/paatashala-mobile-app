import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransportAttendanceBcPage } from './transport-attendance-bc.page';

const routes: Routes = [
  {
    path: '',
    component: TransportAttendanceBcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransportAttendanceBcPageRoutingModule {}

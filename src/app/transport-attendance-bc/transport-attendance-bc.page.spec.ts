import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransportAttendanceBcPage } from './transport-attendance-bc.page';

describe('TransportAttendanceBcPage', () => {
  let component: TransportAttendanceBcPage;
  let fixture: ComponentFixture<TransportAttendanceBcPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TransportAttendanceBcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

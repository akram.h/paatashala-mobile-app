import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parent-message-box',
  templateUrl: './parent-message-box.page.html',
  styleUrls: ['./parent-message-box.page.scss'],
})

export class ParentMessageBoxPage implements OnInit {
  SelectedStudentData:any;
  StudentLoggedData:any;
  RequestMsgObj:any;
  index:any;
  count:any;
  MsgContent:any;
  flag :boolean = false;
  MessageShowContent:any;
  ShowMsg:boolean;
  NoShowMsg:boolean;
  constructor(public navCtrl: NavController, private appserviceProvider:AppserviceProvider,private route: ActivatedRoute) {
    this.SelectedStudentData = JSON.parse(localStorage.getItem("CurrentStudent") || '{}');
    this.index = 0;
    this.count = 10;
    this.RequestMsgObj = {
      StudentId:'',
      OrgId:'',
      Index: '',
      Count: ''
    };
    this.MsgContent=[];
    this.ShowMsg= false;
        this.NoShowMsg= true;
  }
  
  getAvatarImage(type: string): string {
    return type === 'SOME_TYPE' ? 'assets/imgs/mail.png' : 'default_image_url.png';
  }
  shouldHighlight(msg: any): boolean {
    return !msg.Response && (!!msg.Type && msg.Type !== 'NONE');
  }  
  ngOnInit() {
    this.LoadMessages();
  }
  LoadMessages(){
    debugger;
    this.RequestMsgObj.StudentId = 50125;
    this.RequestMsgObj.OrgId = this.SelectedStudentData.OrgId;
    this.RequestMsgObj.Index=this.index;
    this.RequestMsgObj.Count=this.count;
this.appserviceProvider.getParentMessage(this.RequestMsgObj).subscribe(
  data=>{
    debugger;
    if(data == null){
      debugger;
this.flag=true;
    }else{
      debugger;
      this.flag=false;
      this.MsgContent=this.MsgContent.concat(data);
      this.MsgContent.sort((a: any,b: any) => {
        let x= new Date(a.CreatedOn);
        let y = new Date(b.CreatedOn);
         return x>y?1 : x<y?-1 : 0;
        } );
      this.index = this.MsgContent.length + 1;

    }
  }
);
  }
  doRefresh(refresher: any){
    debugger;
    this.count = this.MsgContent.length;
this.LoadMessages();
this.MsgContent=[];
setTimeout(()=>{
  refresher.complete();

},2000);
this.count=10;
  }

  ShowMessage(Id: any)
  {
    debugger;
    this.ShowMsg= true;
    this.NoShowMsg= false;
    this.MessageShowContent={};
    this.MsgContent.some((element: MessageType) => { if(element.Id==Id){
      this.MessageShowContent = element;
    }
    });
console.log(this.MessageShowContent);

  }
  back(){
    this.ShowMsg= false ;
      this.NoShowMsg=true ;
  }

  submitResponse(){

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentMessageBoxPage');
  }


  

}

interface MessageType {
  Id: number;
  
}

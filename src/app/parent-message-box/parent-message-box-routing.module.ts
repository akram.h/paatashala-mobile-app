import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentMessageBoxPage } from './parent-message-box.page';

const routes: Routes = [
  {
    path: '',
    component: ParentMessageBoxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentMessageBoxPageRoutingModule {}

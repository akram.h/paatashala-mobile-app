import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentMessageBoxPage } from './parent-message-box.page';

describe('ParentMessageBoxPage', () => {
  let component: ParentMessageBoxPage;
  let fixture: ComponentFixture<ParentMessageBoxPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentMessageBoxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

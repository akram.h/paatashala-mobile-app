import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentMessageBoxPageRoutingModule } from './parent-message-box-routing.module';

import { ParentMessageBoxPage } from './parent-message-box.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentMessageBoxPageRoutingModule
  ],
  declarations: [ParentMessageBoxPage]
})
export class ParentMessageBoxPageModule {}

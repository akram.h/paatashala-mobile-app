import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentHolidaysPage } from './parent-holidays.page';

const routes: Routes = [
  {
    path: '',
    component: ParentHolidaysPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentHolidaysPageRoutingModule {}

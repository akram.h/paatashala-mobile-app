import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
@Component({
  selector: 'app-parent-holidays',
  templateUrl: './parent-holidays.page.html',
  styleUrls: ['./parent-holidays.page.scss'],
})
export class ParentHolidaysPage implements OnInit {
  ParentLoggedData:any;
  Holidaydetail:HolidayList[];
  DBHolidayDetail: any;
   tempdata:any;
  constructor(public navCtrl: NavController,private AppserviceProvider:AppserviceProvider) {
    this.ParentLoggedData = JSON.parse(localStorage.getItem("ParentLoggedData") || "null");
    this.DBHolidayDetail = [];
    this.Holidaydetail=[];
  }
  ngOnInit(){
    this.getStudentHolidays();
   }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentHolidaysPage');
  }

  getStudentHolidays(){
    debugger;
  this.AppserviceProvider.getStudentHolidays(this.ParentLoggedData.OrgId).subscribe(data =>{
    debugger;
    this.DBHolidayDetail = data;
  this.DBHolidayDetail.forEach((element :any) => {
  let month = this.Holidaydetail.find(res=>res.MonthName==element.MonthName);
  if(month==null||month==undefined)
  {
    let newMonth = new HolidayList();
    newMonth.MonthName = element.MonthName;
    let holiday = new HolidayObj();
    holiday.Date = element.Date;
    holiday.Name = element.HolidayName;
    newMonth.HolidayObjList = [];
    newMonth.HolidayObjList.push(holiday);
    this.Holidaydetail.push(newMonth);
  }else{
    let holiday = new HolidayObj();
    holiday.Date = element.Date;
    holiday.Name = element.HolidayName;
    month.HolidayObjList.push(holiday)
  }
  });
  })
  }


  

}
class HolidayList
{

  MonthName!: string;
  HolidayObjList!: HolidayObj[];
}

class HolidayObj{
  Date : any;
  Name!: string;
}

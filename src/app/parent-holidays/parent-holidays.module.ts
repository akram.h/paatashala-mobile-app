import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentHolidaysPageRoutingModule } from './parent-holidays-routing.module';

import { ParentHolidaysPage } from './parent-holidays.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentHolidaysPageRoutingModule
  ],
  declarations: [ParentHolidaysPage]
})
export class ParentHolidaysPageModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentHolidaysPage } from './parent-holidays.page';

describe('ParentHolidaysPage', () => {
  let component: ParentHolidaysPage;
  let fixture: ComponentFixture<ParentHolidaysPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentHolidaysPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StudentAttendanceMPage } from './student-attendance-m.page';

describe('StudentAttendanceMPage', () => {
  let component: StudentAttendanceMPage;
  let fixture: ComponentFixture<StudentAttendanceMPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StudentAttendanceMPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

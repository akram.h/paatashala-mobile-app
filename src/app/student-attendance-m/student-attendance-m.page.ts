import { Component, OnInit } from '@angular/core';
import { NavController} from '@ionic/angular'; 
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-student-attendance-m',
  templateUrl: './student-attendance-m.page.html',
  styleUrls: ['./student-attendance-m.page.scss'],
})
export class StudentAttendanceMPage implements OnInit {
  AttendenceObj: any;
  EmployeeLoggedData: any;
  BatchCourseObject: any;
  NextPageDiv: boolean;
  SubmittingAttendanceObject: any;
  BatchList: any;
  CourseList: any;
  StudObject: any;
  NextPage: any;
  StudentsList:any;
  AttendanceMarkAll: any;
  SearchStudent: any;
  ResultObject: any;

  constructor(public navCtrl: NavController,private route: ActivatedRoute, private appserviceProvider:AppserviceProvider) {
    this.route.params.subscribe(params => {
      const myParam = params['myParam'];
      // Use the parameter as needed
    });
    this.AttendenceObj = {};
    this.BatchCourseObject = {};
    this.EmployeeLoggedData = {};
    this.SubmittingAttendanceObject = {};
    this.NextPage = {};
    this.StudObject = {};
    this.ResultObject = {};
    this.BatchList = [];
    this.CourseList = [];
    this.StudentsList = [];
    this.SearchStudent = [];
    this.AttendanceMarkAll = [
      { Id: true, Name: 'Present'},
      { Id: false, Name: 'Absent'}
    ];
    this.NextPageDiv = false;

    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") || '{}');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StudentAttendanceMPage');
    this.getBatchCourse();
  }
  getBatchCourse() {
    this.appserviceProvider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
  data => {
    this.BatchCourseObject = data;
    this.BatchList = this.BatchCourseObject.Batches;
    this.CourseList = this.BatchCourseObject.Courses;
}
    );
  }
  getStudent() {
    debugger;
      this.SearchStudent = [];
      this.NextPageDiv = true;
      this.StudObject.BatchId = this.AttendenceObj.BatchId;
      this.StudObject.CourseId = this.AttendenceObj.CourseId;
      this.StudObject.OrgId = this.EmployeeLoggedData.OrgId;
      this.StudObject.AttendanceDate = this.AttendenceObj.AttendanceDate.toString();
      this.appserviceProvider.getStudent(this.StudObject).subscribe(
        data => {
          this.StudentsList = data;
          this.SearchStudent = this.StudentsList;
          console.log(this.StudentsList);
          debugger;
        },
        err =>
        {
          debugger;
        }
      );
  }
  SubmittingAttendance() {
    debugger;
    this.SubmittingAttendanceObject.DailyAttendanceObj = this.StudentsList;
    this.SubmittingAttendanceObject.dateAttendance = this.AttendenceObj.AttendanceDate.toString();
    this.SubmittingAttendanceObject.OrgId = this.EmployeeLoggedData.OrgId;
    this.appserviceProvider.SubmittingAttendance(this.SubmittingAttendanceObject).subscribe(
      data => {
        debugger;
        this.ResultObject = data;
        if(this.ResultObject.status === true) {
          this.appserviceProvider.showAlert('Saved Successfully','');
        } else {
          this.appserviceProvider.showAlert('Somthing went Wrong','');
        }
      },
      err => {
        debugger;
      }
    );
  }
  onCancel() {
    debugger;
    this.SearchStudent = [];
  }
  MarkAllStud() {
    debugger;
    this.StudentsList.forEach((element: any) => {
      element.isPresent = this.NextPage.Mark;
    });
  }
  SearchStudentName(Event: any) {
    debugger;
    this.StudentsList = this.SearchStudent;
    const val = Event.target.value;
    if (val && val.trim() != '') {
      this.StudentsList = this.StudentsList.filter((item: any) => {
        return (item.StudentName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  keySearch() {
    debugger;
  }

  ngOnInit() {
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentAttendanceMPage } from './student-attendance-m.page';

const routes: Routes = [
  {
    path: '',
    component: StudentAttendanceMPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentAttendanceMPageRoutingModule {}

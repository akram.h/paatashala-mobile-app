import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentAttendanceMPageRoutingModule } from './student-attendance-m-routing.module';

import { StudentAttendanceMPage } from './student-attendance-m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentAttendanceMPageRoutingModule
  ],
  declarations: [StudentAttendanceMPage]
})
export class StudentAttendanceMPageModule {}

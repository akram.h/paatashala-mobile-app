import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentTrackStudentPageRoutingModule } from './parent-track-student-routing.module';

import { ParentTrackStudentPage } from './parent-track-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentTrackStudentPageRoutingModule
  ],
  declarations: [ParentTrackStudentPage]
})
export class ParentTrackStudentPageModule {}

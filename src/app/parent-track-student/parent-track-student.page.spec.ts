import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentTrackStudentPage } from './parent-track-student.page';

describe('ParentTrackStudentPage', () => {
  let component: ParentTrackStudentPage;
  let fixture: ComponentFixture<ParentTrackStudentPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentTrackStudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { GoogleMaps, GoogleMap, GoogleMapOptions, Marker, GoogleMapsEvent, Environment } from '@ionic-native/google-maps/ngx'; // Import from '@ionic-native/google-maps/ngx'

@Component({
  selector: 'app-parent-track-student',
  templateUrl: './parent-track-student.page.html',
  styleUrls: ['./parent-track-student.page.scss'],
})
export class ParentTrackStudentPage implements OnInit {
  map!: GoogleMap;
  ParentLoggedData: any;
  RouteCode: any[] = []; // Initialize as an empty array
  selectedRoute: string = ''; // Initialize with an empty string
  GetloCationObj: any = {};
  LocData: any;
  interval: any;

  constructor(
    public navCtrl: NavController,
    private route: ActivatedRoute,
    private appServiceProvider: AppserviceProvider
  ) {
    this.ParentLoggedData = JSON.parse(localStorage.getItem("ParentLoggedData") || '{}');
    this.GetloCationObj = {};
  }

  async getRouteCode() {
    try {
      const data = await this.appServiceProvider.getRoutecode(this.ParentLoggedData.OrgId).toPromise();
      this.RouteCode = data as any[]; // Use a type assertion here
    } catch (error) {
      console.error(error);
    }
  }

  StartLocating() {
    console.log(this.selectedRoute);
    this.getRouteLocation();
    this.interval = setInterval(() => {
      this.getRouteLocation();
      this.map.clear();
      this.map.addMarker({
        title: 'Ionic',
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: this.LocData.Latitude,
          lng: this.LocData.Longitude
        }
      });
    }, 20000);
  }

  async getRouteLocation() {
    console.log(this.selectedRoute);
    this.GetloCationObj.Routecode = this.selectedRoute;
    this.GetloCationObj.OrgId = this.ParentLoggedData.OrgId;
    try {
      const data = await this.appServiceProvider.GetLocation(this.GetloCationObj).toPromise();
      this.LocData = data;
    } catch (error) {
      console.error(error);
    }
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter ParentTrackStudentPage');
    this.loadMap();
    this.getRouteCode();
  }

  loadMap() {
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_DEBUG': 'YOUR_DEBUG_API_KEY',
      'API_KEY_FOR_BROWSER_RELEASE': 'YOUR_RELEASE_API_KEY'
    });

    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 12.9716,
          lng: 77.5946
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    this.map.addMarker({
      title: 'Ionic',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: 12.9716,
        lng: 77.5946
      }
    }).then(marker => {
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        alert('clicked');
      });
    });
  }

  ionViewWillLeave() {
    clearInterval(this.interval);
  }

  ngOnInit() {
  }
}

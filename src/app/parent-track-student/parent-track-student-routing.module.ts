import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentTrackStudentPage } from './parent-track-student.page';

const routes: Routes = [
  {
    path: '',
    component: ParentTrackStudentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentTrackStudentPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';
//import { Geolocation } from '@ionic-native/geolocation';
import { GeoLocationPageRoutingModule } from './geo-location-routing.module';
import { Geolocation } from '@ionic-native/geolocation';
import { GeoLocationPage } from './geo-location.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
   
    GeoLocationPageRoutingModule
  ],
  declarations: [GeoLocationPage],
  providers: [AppserviceProvider]
})
export class GeoLocationPageModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GeoLocationPage } from './geo-location.page';

describe('GeoLocationPage', () => {
  let component: GeoLocationPage;
  let fixture: ComponentFixture<GeoLocationPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(GeoLocationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

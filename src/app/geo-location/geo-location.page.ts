import { Component, OnInit,OnDestroy } from '@angular/core';
import { NavController,NavParams,Platform } from '@ionic/angular';

import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
@Component({
  selector: 'app-geo-location',
  templateUrl: './geo-location.page.html',
  styleUrls: ['./geo-location.page.scss'],
})
export class GeoLocationPage implements OnInit {
  location: any;
  EmployeeLoggedData: any;
  RouteCode: any;
  LocationObject: any;
  sub !: Subscription;
  constructor(public appservice: AppserviceProvider, private geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams, public platform:Platform) {
    this.location = {};
    this.EmployeeLoggedData = {};
    this.RouteCode = [];
    this.LocationObject = {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GeoLocationPage');
    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") || "{}");

    this.GetRouteCode();
  }

  GetRouteCode() {
  this.appservice.GetRouteCode(this.EmployeeLoggedData.OrgId).subscribe(
    data => {
      this.RouteCode = data;
    }
  );
  }

  getCurrentLocation() {
    debugger;
    const options = { timeout: 10000, enableHighAccuracy: true };
    this.geolocation.getCurrentPosition((response: GeolocationPosition) => {
      debugger;
      this.location.Latitude = response.coords.latitude;
       this.location.Longitude = response.coords.longitude;
      this.appservice.UpdateRouteLocation(this.location).subscribe();
      // this.base.presentAlert("Location Updated");
    }, 
    (error: GeolocationPositionError) => {
      console.error('Error occurred while getting geolocation:', error);
    },
    options
    );
  }

  TrackLocation() {
    debugger;
    if(this.LocationObject.ShareLocation === true) {
      this.location.RouteCode = this.LocationObject.RouteCode;
      this.location.OrgId = this.EmployeeLoggedData.OrgId;

      // this.sub = Observable.interval(10000).subscribe(() => { this.getCurrentLocation() });
      // this.getCurrentLocation();
    }
    else {
      this.sub.unsubscribe();
    }

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }



  ngOnInit() {
  }

}

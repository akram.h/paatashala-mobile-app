import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnquiryFormPage } from './enquiry-form.page';

const routes: Routes = [
  {
    path: '',
    component: EnquiryFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnquiryFormPageRoutingModule {}

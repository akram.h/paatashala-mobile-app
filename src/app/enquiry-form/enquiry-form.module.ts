import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';

import { EnquiryFormPageRoutingModule } from './enquiry-form-routing.module';

import { EnquiryFormPage } from './enquiry-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnquiryFormPageRoutingModule
  ],
  declarations: [EnquiryFormPage],
  providers:[AppserviceProvider],
})
export class EnquiryFormPageModule {}

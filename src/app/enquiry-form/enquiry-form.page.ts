import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';

@Component({
  selector: 'app-enquiry-form',
  templateUrl: './enquiry-form.page.html',
  styleUrls: ['./enquiry-form.page.scss'],
})
export class EnquiryFormPage implements OnInit {
  // Gender: any;
  LeadDetails: any;
  StudentDetails: any;
  AcadamicDetails: any;
  LeadDetailObject: any;
  EmployeeLoggedData : any;
    StreamList: any;
    AdmissionStatuslist: any;
    EmployeeList: any;
    OtherProgramList: any;
    BatchCourseObject: any;
    BatchList: any;
    CourseList: any;
    Result: any;
    SaveObject: any;
    constructor(public navCtrl: NavController,  private appserviceProvider:AppserviceProvider) {
      // this.Gender = [{ Value: 1, Name: 'Male' },
      //                { Value: 2, Name: 'Female' }];
      this.Result = {};
      this.LeadDetails = {};
      this.StudentDetails = {};
      this.AcadamicDetails = {};
      this.LeadDetailObject= {};
      this.BatchCourseObject = {};
      this.SaveObject = {};
      this.StreamList=[];
      this.AdmissionStatuslist = [];
      this.EmployeeList = [];
      this.OtherProgramList = [];
      this.BatchList = [];
      this.CourseList = [];
  
      this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") as any);
      //debugger;
  
    }
  
    ngOnInit(){
     this.getLeadDetails();
     this.getBatchCourse();
     }
  
    ionViewDidLoad() {
  
      //console.log(this.EmployeeLoggedData);
  
    }
  
  
    getLeadDetails() {
      //debugger;
      
      this.appserviceProvider.getLeadDetails(this.EmployeeLoggedData.OrgId).subscribe(
        data => {
          this.LeadDetailObject = data;
          this.StreamList = this.LeadDetailObject.Streams;
          this.AdmissionStatuslist = this.LeadDetailObject.AdmStatus;
          this.EmployeeList = this.LeadDetailObject.EmployeesList;
          this.OtherProgramList = this.LeadDetailObject.OtherPrograms;
          console.log(data);
        }
      );
    

  }
  
    getBatchCourse() {
      this.appserviceProvider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
    data => {
      this.BatchCourseObject = data;
      this.BatchList = this.BatchCourseObject.Batches;
      this.CourseList = this.BatchCourseObject.Courses;
  }
      );
    }
  
    SaveDetails() {
  
      this.SaveObject.student = this.StudentDetails;
      this.SaveObject.registration = this.AcadamicDetails;
      this.SaveObject.LeadFollowUp = this.LeadDetails;
      this.SaveObject.OrgId = this.EmployeeLoggedData.OrgId;
      this.SaveObject.EmployeeId = this.EmployeeLoggedData.EmpId;
      this.appserviceProvider.SaveEnquiry(this.SaveObject).subscribe(
        data => {
        //  debugger;
          this.Result = data;
          if(this.Result.status == true)
          this.appserviceProvider.showAlert('Saved Successfully','');
          else
          this.appserviceProvider.showAlert('Failed', this.Result.message);
        }
      );
      // this.appserviceProvider.SaveDetails(this.StudentDetails, this.AcadamicDetails, this.LeadDetails, this.EmployeeLoggedData.OrgId, this.EmployeeLoggedData.EmpId).subscribe(
      //   data => {
        //   this.Result = data;
        //   if(this.Result.status == true)
        //   this.appserviceProvider.showAlert('Saved Successfully','');
        //   else
        //   this.appserviceProvider.showAlert('Failed', this.Result.message);
        // }
      // );
    }
  
  
 

}

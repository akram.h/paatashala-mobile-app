import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EnquiryFormPage } from './enquiry-form.page';

describe('EnquiryFormPage', () => {
  let component: EnquiryFormPage;
  let fixture: ComponentFixture<EnquiryFormPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EnquiryFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

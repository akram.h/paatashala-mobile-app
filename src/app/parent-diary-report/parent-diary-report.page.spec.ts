import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentDiaryReportPage } from './parent-diary-report.page';

describe('ParentDiaryReportPage', () => {
  let component: ParentDiaryReportPage;
  let fixture: ComponentFixture<ParentDiaryReportPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentDiaryReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentDiaryReportPageRoutingModule } from './parent-diary-report-routing.module';

import { ParentDiaryReportPage } from './parent-diary-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentDiaryReportPageRoutingModule
  ],
  declarations: [ParentDiaryReportPage]
})
export class ParentDiaryReportPageModule {}

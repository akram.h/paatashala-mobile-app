import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentDiaryReportPage } from './parent-diary-report.page';

const routes: Routes = [
  {
    path: '',
    component: ParentDiaryReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentDiaryReportPageRoutingModule {}

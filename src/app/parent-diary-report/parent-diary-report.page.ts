import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
@Component({
  selector: 'app-parent-diary-report',
  templateUrl: './parent-diary-report.page.html',
  styleUrls: ['./parent-diary-report.page.scss'],
})
export class ParentDiaryReportPage implements OnInit {
  localData:any;
  studentData:any;
  DairyData:any;
  constructor(public navCtrl: NavController, private AppserviceProvider:AppserviceProvider) {
    debugger;
    this.localData={};
    this.localData = JSON.parse(localStorage.getItem('CurrentStudent')!);
    this.studentData = this.localData;
    this.DairyData= [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentDiaryReportPage');
    this.getDairyReports();

  }

  getDairyReports()
  {
    this.AppserviceProvider.getDairyReports(this.studentData).subscribe(
      data=>{
        debugger;

        this.DairyData = data;
        console.log(this.DairyData);
      },
      err=>{

      }

    )
  }




  ngOnInit() {
  }

}

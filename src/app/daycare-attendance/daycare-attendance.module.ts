import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaycareAttendancePageRoutingModule } from './daycare-attendance-routing.module';

import { DaycareAttendancePage } from './daycare-attendance.page';
import { AppserviceProvider } from 'src/providers/appservice/appservice';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaycareAttendancePageRoutingModule
  ],
  declarations: [DaycareAttendancePage],
  providers: [AppserviceProvider] 
})
export class DaycareAttendancePageModule {}

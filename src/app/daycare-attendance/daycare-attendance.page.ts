import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { from } from 'rxjs';  
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-daycare-attendance',
  templateUrl: './daycare-attendance.page.html',
  styleUrls: ['./daycare-attendance.page.scss'],
})
export class DaycareAttendancePage implements OnInit {
  DayCareObject: any;
  BatchList: any;
  CourseList: any;
  EmployeeLoggedData: any;
  BatchCourseObject: any;
  NextPageDiv: boolean;
  NextPage: any;
  StudentsList: any;

    constructor(public navCtrl: NavController,private route: ActivatedRoute, private appserviceProvider: AppserviceProvider) 
    {
      this.DayCareObject = {};
      this.EmployeeLoggedData = {};
      this.BatchCourseObject = {};
      this.NextPage = {};
      this.StudentsList = [];
      this.BatchList = [];
      this.CourseList = [];
      this.NextPageDiv = false;
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad DaycareAttendancePage');
      this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") ?? '{}');

      this.getBatchCourse();
    }
    getBatchCourse() {
      this.appserviceProvider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe((data) => {
        this.BatchCourseObject = data;
        this.BatchList = this.BatchCourseObject.Batches;
        this.CourseList = this.BatchCourseObject.Courses;
      });
    }
    getStudent() {
      this.DayCareObject.OrgId = this.EmployeeLoggedData.OrgId;
      this.appserviceProvider.GetDaycareStudentsAttendance(this.DayCareObject).subscribe((data) => {
        this.NextPageDiv = true;
        this.StudentsList = data;
        debugger;
      });
    }
    SearchStudentName($event: any) {
      // Explicitly specify the type of $event
    }
  
    ngOnInit() {
      // Implement ngOnInit if needed
    }

}

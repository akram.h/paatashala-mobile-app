import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DaycareAttendancePage } from './daycare-attendance.page';

describe('DaycareAttendancePage', () => {
  let component: DaycareAttendancePage;
  let fixture: ComponentFixture<DaycareAttendancePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DaycareAttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

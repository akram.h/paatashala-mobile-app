import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentAttendanceBcPage } from './student-attendance-bc.page';

const routes: Routes = [
  {
    path: '',
    component: StudentAttendanceBcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentAttendanceBcPageRoutingModule {}

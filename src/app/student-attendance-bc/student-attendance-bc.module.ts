import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { StudentAttendanceBcPageRoutingModule } from './student-attendance-bc-routing.module';

import { StudentAttendanceBcPage } from './student-attendance-bc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentAttendanceBcPageRoutingModule
  ],
  declarations: [StudentAttendanceBcPage],
  providers:
  [
    AppserviceProvider,
    BarcodeScanner,
   ],
})
export class StudentAttendanceBcPageModule {}

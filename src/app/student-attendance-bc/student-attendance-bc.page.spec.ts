import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StudentAttendanceBcPage } from './student-attendance-bc.page';

describe('StudentAttendanceBcPage', () => {
  let component: StudentAttendanceBcPage;
  let fixture: ComponentFixture<StudentAttendanceBcPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StudentAttendanceBcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

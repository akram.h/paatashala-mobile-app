import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-student-attendance-bc',
  templateUrl: './student-attendance-bc.page.html',
  styleUrls: ['./student-attendance-bc.page.scss'],
})
export class StudentAttendanceBcPage implements OnInit {
  StudentList:any;
LoggedEmployeeData:any;
scannedStudentList:any;
date:any;
pick:any;
stdBCAttendanceData:any;
  result: any;
  constructor(public navCtrl: NavController, private route: ActivatedRoute,private appserviceProvider:AppserviceProvider,private barcodeScanner: BarcodeScanner) {
    const parameterValue = this.route.snapshot.params['parameterName'];
    this.StudentList = [];
    this.LoggedEmployeeData = JSON.parse(localStorage.getItem("EmployeeLoggedData") || '{}');
    this.scannedStudentList = {
      Id: [],
      Name: [],
      StudentId: []
    }
    this.date= new Date();
    this.stdBCAttendanceData={};
    this.pick = null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StudentAttendanceBcPage');
  }

  ngOnInit() {
    this.getsTudentList();
  }

  ScanBarCode() {
    // Use the BarcodeScanner service here
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
    }).catch(err => {
      console.log('Error', err);
    });
  }


getsTudentList(){
this.appserviceProvider.GetStudentList(this.LoggedEmployeeData.OrgId).subscribe(
  data=>{
    debugger;
    console.log(data);
    this.StudentList = data;
  }
);
  }


  DeleteCurrentRow(index : any) {
    this.scannedStudentList.Name.splice(index, 1);
    this.scannedStudentList.Id.splice(index, 1);
    this.scannedStudentList.StudentId.splice(index, 1);
  }

SendStudentAtt(){
  this.stdBCAttendanceData.OrgId = this.LoggedEmployeeData.OrgId;
  this.stdBCAttendanceData.StudentId =  this.scannedStudentList.Id.toString();
  this.stdBCAttendanceData.scanDateTime = this.date.toString();
  this.stdBCAttendanceData.IsCheckIn = this.pick;
  debugger;
this.appserviceProvider.sendStudentAttnd(this.stdBCAttendanceData).subscribe(
data=>{
  debugger;
  this.result=data;
  if(this.result.status==true){
    this.appserviceProvider.showAlert('Saved successfully ','');
  this.scannedStudentList = {
    Id: [],
    Name: [],
    EmployeeId: []
  }
} else
{
  this.appserviceProvider.showAlert('Failed',this.result.message);
}
}
);
}

 
}

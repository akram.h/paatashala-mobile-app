import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { Camera } from '@ionic-native/camera';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { IonicModule } from '@ionic/angular';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path/ngx';
import { GalleryPageRoutingModule } from './gallery-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { GalleryPage } from './gallery.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GalleryPageRoutingModule,
    
  ],
  declarations: [GalleryPage],
  providers:[Camera,
    FilePath,
  ],
})
export class GalleryPageModule {}

import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions, PictureSourceType, DestinationType } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})

export class GalleryPage implements OnInit {
  EmployeeLoggedData: any;
  BatchObject: any;
  batchList: any;
  courseList: any;
  GalleryObject:any;


imageURI:any;
imageFileName:any;


constructor(
  private appserviceprovider: AppserviceProvider,
  public navCtrl: NavController,
  public navParams: NavParams,
  private camera: Camera,
  private formBuilder: FormBuilder,
  @Inject(FilePath) private filePath: typeof FilePath,
  public actionSheetCtrl: ActionSheetController,
  public toastCtrl: ToastController,
  public platform: Platform,
  private route: ActivatedRoute,
  private file: File,
  public loadingCtrl: LoadingController,
  @Inject(FileTransfer) private filetransfer: typeof FileTransfer // Add the FileTransfer service here
) {
  // this.route.params.subscribe(params => {
  //   const myParam = params['myParam'];
  //   // Use the parameter as needed
  // });
  this.camera = camera;
  this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData")?.toString()??"");
  this.BatchObject={};
  this.batchList=[];
  this.courseList=[];
  this.GalleryObject={};
  }
   async ngOnInit() {
     await this.getBatchandCourse();
  }
  getBatchandCourse() {
    debugger;
    this.appserviceprovider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
      data=>{

        this.BatchObject = data;
        this.batchList = this.BatchObject.Batches;
        this.courseList = this.BatchObject.Courses;
      }
    );
  }
  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: DestinationType.FILE_URL,
      sourceType: PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData: any) => {
      this.imageURI = 'data:image/jpeg;base64,' + imageData;
      debugger;
    }, (err: any) => {
      debugger;
      console.log(err);
      this.presentToast(err);
    });
  }
  takeImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData: any) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.imageURI =  imageData;
    }, (err:any) => {
     // Handle error
     console.log(err);
     this.presentToast(err);
    });
  }


   async uploadFile() {
    let loader = await this.loadingCtrl.create({
      message: "Uploading..." });
    debugger;
     await loader.present();

    
    const fileTransfer: FileTransferObject = this.filetransfer.create();

   
    debugger;
    let options : FileUploadOptions = {
    fileKey : 'image_file',
    fileName : this.imageURI.substr(this.imageURI.lastIndexOf('/')+1),
    chunkedMode : false,
    mimeType : "image/jpeg",
    httpMethod : "POST",
     headers : {},
     params : {
      "BatchId" : this.GalleryObject.BatchId,
      "CourseId" : this.GalleryObject.CourseId,
      "OrgId" : this.EmployeeLoggedData.OrgId,
      "EmpId" : this.EmployeeLoggedData.EmpId
              }

    }
    console.log(this.imageURI);
    fileTransfer.upload(this.imageURI, 'Global.BASE_API_URL/Gallery/UploadImage', options)
      .then((data) => {
        debugger;
      console.log(data+" Uploaded Successfully");
      loader.dismiss();
      this.presentToast("Image uploaded successfully");
      
    
   }, (err) =>  {
      debugger;
      console.log(err);
      loader.dismiss();
      this.presentToast(err);
    
  });

     }

   
 async  presentToast(msg: any) {
      const toast =  await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss().then(() => {
      console.log('Dismissed toast');
    });
  

     await toast.present();
    
  }
  ionViewDidLoad() {
   console.log('ionViewDidLoad GalleryPage');
     }

}

 


function IonicPage(): (target: typeof GalleryPage) => void | typeof GalleryPage {
  throw new Error('Function not implemented.');
}


import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
  import { NavController,NavParams} from '@ionic/angular';
  import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { BatchCourseData } from 'src/interfaces/BatchCourseData';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-activity-report',
  templateUrl: './activity-report.page.html',
  styleUrls: ['./activity-report.page.scss'],
})
export class ActivityReportPage implements OnInit {
  ActivityReport:any;
  EmployeeLoggedData:any;
  ActType:any;
  result: any;
  BatchCourseObj: BatchCourseData;
  AddAct: any;
  StudList: any;
  Soption:any;
  AddStud: any;
  ActTypeList:any;
  ActSummary: any;
  pageOne:any;
  pageTwo:any;
  pageThree:any;
  tempList: any;
  constructor(public navCtrl: NavController, private route: ActivatedRoute,private appserviceProvider:AppserviceProvider) {
    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData")??"{}");
    const emptyBatchCourseData: BatchCourseData = {
      Batches: [],
      Courses: []
    };
    this.ActType = {};
    this.BatchCourseObj=emptyBatchCourseData;
    this.AddAct ={};
    this.StudList=[];
    this.tempList=[];
    this.Soption=[];
    this.AddStud={};
    this.ActTypeList=[];
    this.ActSummary={};
    this.pageOne = false;
    this.pageTwo = true;
    this.pageThree = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityReportPage');
  }
 ngOnInit() {
   this.ActivityReport = "Type";
   this.GetBatchCourseList();
   this.Soption=[
     {Id:true,Name:'All'},
     {Id:false,Name:'None'}
   ];
   this.AddAct.AttendanceDate = new Date().toISOString();
 }
 AddNewAct(){
this.ActType.OrgId = this.EmployeeLoggedData.OrgId;
this.appserviceProvider.addNewact(this.ActType).subscribe(
  data=>{
    debugger;
    this.result=data;
    if(this.result.status==true){
      this.appserviceProvider.showAlert('Saved successfully ','');
      this.ActType = {
        OrgId : this.EmployeeLoggedData.OrgId,
        Name:'',
        Description:''
      };
    }else
    {
this.appserviceProvider.showAlert('Failed',this.result.message);
    }
  }
);
 }

 GetBatchCourseList() {
   this.appserviceProvider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
     (data: BatchCourseData)=>{
      this.BatchCourseObj = data;
     }
   );
 }

 getStud(){
   this.AddAct.OrgId=this.EmployeeLoggedData.OrgId;
   this.appserviceProvider.getStudentListBasedOnFiler(this.AddAct).subscribe(
     data=>{
       debugger;
       this.StudList=data;
       this.tempList=data;
       this.pageOne = true;
    this.pageTwo = false;
    this.pageThree = true;
     }
   );
 }

 Select() {
  this.StudList.forEach((element: { isPresent: any; }) => {
    element.isPresent=this.AddStud.slct;
 });
 }
 OnAddngStd(){
   this.getActivityTypes();
   this.pageOne = true;
   this.pageTwo = true;
   this.pageThree = false;
 }

 getActivityTypes(){
   debugger;
   this.appserviceProvider.GetActTypes(this.EmployeeLoggedData.OrgId).subscribe(
     data=>{
this.ActTypeList=data;
     }
   );
 }

 FinalSubmit(){

this.ActSummary.StudentListObj = this.StudList;
this.ActSummary.ActivityDate = this.AddAct.AttendanceDate;
this.ActSummary.OrgId = this.EmployeeLoggedData.OrgId;
this.ActSummary.BatchId = this.AddAct.BatchId;
this.ActSummary.CourseId = this.AddAct.CourseId;
debugger;
this.appserviceProvider.saveStudentActivity(this.ActSummary).subscribe(
  data=>{
    debugger;
    this.result=data;
    if(this.result.status==true){
      this.appserviceProvider.showAlert('Saved successfully ','');
     /* this.ActSummary = {
        WhatDid:'',
        HowMuch:'',
        When:'',
        Comments:'',
        ActivityId:''
      };*/
      this.pageOne = false;
      this.pageTwo = true;
      this.pageThree = true;
      this.AddAct={
        AttendanceDate:new Date().toISOString(),
        BatchId:'',
        CourseId:''
      };

    }else
    {
this.appserviceProvider.showAlert('Failed',this.result.message);
    }

  }
);
 }

 onInput(search: any){
   debugger;
   console.log(search.target.value);
  this.StudList = this.tempList;
  if(search.target.value != "") {
    this.StudList = this.StudList.filter((item: { StudentName: string; }) => {
      return (item.StudentName.toLowerCase().indexOf(search.target.value.toLowerCase()) > -1);
      });
    }
    }

}
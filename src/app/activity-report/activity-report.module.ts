import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityReportPageRoutingModule } from './activity-report-routing.module';

import { ActivityReportPage } from './activity-report.page';
import { HttpClientModule } from '@angular/common/http';
import { AppserviceProvider } from 'src/providers/appservice/appservice';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ActivityReportPageRoutingModule
  ],
  declarations: [ActivityReportPage],
  providers: [AppserviceProvider]
})
export class ActivityReportPageModule {}

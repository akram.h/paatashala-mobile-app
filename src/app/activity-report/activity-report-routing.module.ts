import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivityReportPage } from './activity-report.page';

const routes: Routes = [
  {
    path: '',
    component: ActivityReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActivityReportPageRoutingModule {}

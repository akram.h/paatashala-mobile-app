import { Component } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public navCtrl: NavController) {}
  goTransportAttendanceBC() {
    this.navCtrl.navigateForward('/transport-attendance-bc');
  }
  goStudentAttendanceBC() {
    this.navCtrl.navigateForward('/student-attendance-bc');
  }
  goemployeeAttendanceBC() {
    this.navCtrl.navigateForward('/employee-attendance-bc');
  }
  goDayCareAttendance() {
    this.navCtrl.navigateForward('/daycare-attendance');
  }
  goGeoLocation() {
    this.navCtrl.navigateForward('/geo-location');
  }
  goStudentAttendanceM() {
    this.navCtrl.navigateForward('/student-attendance-m');
  }
  goTransportAttendanceM() {
    this.navCtrl.navigateForward('/transport-attendance-m');
  }
  goEmployeeAttendanceM() {
    this.navCtrl.navigateForward('/employee-attendance-m');
  }
  goHolidaysE() {
    this.navCtrl.navigateForward('/holidays-e');
  }
  goPersonalDetails(){
    this.navCtrl.navigateForward('/personal-details');
  }
  goGallery() {
    this.navCtrl.navigateForward('/gallery');
  }
  goEnquiryForm() {
    this.navCtrl.navigateForward('/enquiry-form');
  }
  goDairyReportE() {
    this.navCtrl.navigateForward('/diary-report-e');
  }
  goMedicineReport() {
    this.navCtrl.navigateForward('/medicine-report');
  }
  goActivityReport() {
    this.navCtrl.navigateForward('/activity-report');
  }
 
}

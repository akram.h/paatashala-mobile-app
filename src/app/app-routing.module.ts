import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ParentFeeDetailsPage } from './parent-fee-details/parent-fee-details.page';
const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'parent-signup',
    loadChildren: () => import('./parent-signup/parent-signup.module').then( m => m.ParentSignupPageModule)
  },
  {
    path: 'parent-forgotpassword',
    loadChildren: () => import('./parent-forgotpassword/parent-forgotpassword.module').then( m => m.ParentForgotpasswordPageModule)
  },
  {
    path: 'employee-forgotpassword',
    loadChildren: () => import('./employee-forgotpassword/employee-forgotpassword.module').then( m => m.EmployeeForgotpasswordPageModule)
  },
  
  {
    path: 'medicine-report',
    loadChildren: () => import('./medicine-report/medicine-report.module').then( m => m.MedicineReportPageModule)
  },
  {
    path: 'activity-report',
    loadChildren: () => import('./activity-report/activity-report.module').then( m => m.ActivityReportPageModule)
  },
  {
    path: 'daycare-attendance',
    loadChildren: () => import('./daycare-attendance/daycare-attendance.module').then( m => m.DaycareAttendancePageModule)
  },
  
  
  {
    path: 'gallery',
    loadChildren: () => import('./gallery/gallery.module').then( m => m.GalleryPageModule)
  },
  {
    path: 'geo-location',
    loadChildren: () => import('./geo-location/geo-location.module').then( m => m.GeoLocationPageModule)
  },
  {
    path: 'holidays-e',
    loadChildren: () => import('./holidays-e/holidays-e.module').then( m => m.HolidaysEPageModule)
  },
  
  {
    path: 'parent-diary-report',
    loadChildren: () => import('./parent-diary-report/parent-diary-report.module').then( m => m.ParentDiaryReportPageModule)
  },
  {
    path: 'parent-fee-details',
    loadChildren: () => import('./parent-fee-details/parent-fee-details.module').then( m => m.ParentFeeDetailsPageModule)
  },
  {
    path: 'parent-holidays',
    loadChildren: () => import('./parent-holidays/parent-holidays.module').then( m => m.ParentHolidaysPageModule)
  },
  {
    path: 'parent-home',
    loadChildren: () => import('./parent-home/parent-home.module').then( m => m.ParentHomePageModule)
  },
  {
    path: 'parent-message-box',
    loadChildren: () => import('./parent-message-box/parent-message-box.module').then( m => m.ParentMessageBoxPageModule)
  },
  {
    path: 'parent-phtogallery',
    loadChildren: () => import('./parent-phtogallery/parent-phtogallery.module').then( m => m.ParentPhtogalleryPageModule)
  },
  {
    path: 'parent-reports',
    loadChildren: () => import('./parent-reports/parent-reports.module').then( m => m.ParentReportsPageModule)
  },
  {
    path: 'parent-select-student',
    loadChildren: () => import('./parent-select-student/parent-select-student.module').then( m => m.ParentSelectStudentPageModule)
  },
  {
    path: 'parent-student-details',
    loadChildren: () => import('./parent-student-details/parent-student-details.module').then( m => m.ParentStudentDetailsPageModule)
  },
  {
    path: 'parent-track-student',
    loadChildren: () => import('./parent-track-student/parent-track-student.module').then( m => m.ParentTrackStudentPageModule)
  },
  {
    path: 'personal-details',
    loadChildren: () => import('./personal-details/personal-details.module').then( m => m.PersonalDetailsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'student-attendance-bc',
    loadChildren: () => import('./student-attendance-bc/student-attendance-bc.module').then( m => m.StudentAttendanceBcPageModule)
  },
  {
    path: 'student-attendance-m',
    loadChildren: () => import('./student-attendance-m/student-attendance-m.module').then( m => m.StudentAttendanceMPageModule)
  },
  
  
 
  {
    path: 'transport-attendance-bc',
    loadChildren: () => import('./transport-attendance-bc/transport-attendance-bc.module').then( m => m.TransportAttendanceBcPageModule)
  },
  {
    path: 'transport-attendance-m',
    loadChildren: () => import('./transport-attendance-m/transport-attendance-m.module').then( m => m.TransportAttendanceMPageModule)
  },
  {
    path: 'employee-attendance-m',
    loadChildren: () => import('./employee-attendance-m/employee-attendance-m.module').then( m => m.EmployeeAttendanceMPageModule)
  },
  {
    path: 'employee-attendance-bc',
    loadChildren: () => import('./employee-attendance-bc/employee-attendance-bc.module').then( m => m.EmployeeAttendanceBcPageModule)
  },
  {
    path: 'enquiry-form',
    loadChildren: () => import('./enquiry-form/enquiry-form.module').then( m => m.EnquiryFormPageModule)
  },
  {
    path: 'diary-report-e',
    loadChildren: () => import('./diary-report-e/diary-report-e.module').then( m => m.DiaryReportEPageModule)
  },
 

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoginData } from './logindata';

import { HomePage } from '../home/home.page';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { Base } from 'src/shared/base/base';
import { ParentHomePage } from '../parent-home/parent-home.page';
import { ParentForgotpasswordPage } from '../parent-forgotpassword/parent-forgotpassword.page';
import { ParentSignupPage } from '../parent-signup/parent-signup.page';
import { EmployeeForgotpasswordPage } from '../employee-forgotpassword/employee-forgotpassword.page';
import { Api } from 'src/shared/api/api';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: any;
  loginData: any;
  userDetails: any;
  EmployeeData: any;
  ParentData: any;
  LoginType: string = '';
  constructor
    (
      public navCtrl: NavController,

      private appserviceProvider: AppserviceProvider,
      private base: Base,
      private ApiService: Api,
      private route: ActivatedRoute
    ) {
    this.user = {};
    this.loginData = {};
    this.userDetails = {};
    this.EmployeeData = {};
    this.ParentData = {};
  }
  ngOnInit() {
    const loginType = this.route.snapshot.paramMap.get('loginType');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login(loginData: any) {
    if (this.LoginType == "Employee") {
      debugger;
      this.user = {
        Username: loginData.Username, Password: loginData.Password, OrgName: loginData.OrgName
      };
      this.appserviceProvider.checkEmployeelogin(this.user).subscribe(
        data => {
          this.EmployeeData = data;
          if (this.EmployeeData.Status) {
            localStorage.setItem('EmployeeLoggedData', JSON.stringify({
              token: this.EmployeeData.Token,
              Empname: this.EmployeeData.User.Username,
              EmpId: parseInt(this.EmployeeData.User.UserId),
              OrgId: parseInt(this.EmployeeData.User.OrgId),
              role: this.EmployeeData.User.Role
            }));
            debugger;
            this.ApiService.setUserType("Employee");
            this.navCtrl.navigateForward('home');
            this.base.presentToast('Login Suceess', 3500);

          } else {
            this.base.presentToast(this.EmployeeData.Message, 3500);
            return
          }
        },
        err => {
          this.base.presentToast(err.Message, 3500);
          console.error(err);
        }

      );
    } else {
      debugger;
      this.user = {
        Username: loginData.Username, Password: loginData.Password
      };
      this.appserviceProvider.checkParentslogin(this.user).subscribe(
        data => {
          debugger;
          this.ParentData = data;
          if (this.ParentData.Status) {
            localStorage.setItem('ParentLoggedData', JSON.stringify({
              token: this.ParentData.Token,
              Empname: this.ParentData.User.Username,
              EmpId: parseInt(this.ParentData.User.UserId),
              OrgId: parseInt(this.ParentData.User.OrgId),
              ParentStudentList: this.ParentData.HasStudents
            }));
            this.ApiService.setUserType("Parent");
            localStorage.setItem('CurrentStudentIndex', JSON.stringify(0));
            localStorage.setItem('CurrentStudent', JSON.stringify(
              {
                Name: this.ParentData.HasStudents[0].Name,
                StudentId: this.ParentData.HasStudents[0].Id,
                Batch: this.ParentData.HasStudents[0].Batch,
                Course: this.ParentData.HasStudents[0].Course,
                FatherName: this.ParentData.HasStudents[0].FatherName,
                RegistrationCode: this.ParentData.HasStudents[0].RegistrationCode,
                Sex: this.ParentData.HasStudents[0].Sex,
                OrgId: this.ParentData.HasStudents[0].OrgId
              }));
            this.navCtrl.navigateForward('parent-home');
            this.base.presentToast('Login Suceess', 3500);

          } else {
            this.base.presentToast(this.EmployeeData.Message, 3500);
            return
          }
        },
        err => {
          this.base.presentToast(err.Message, 3500);
          console.error(err);
        }

      )
    }



  }

  // login(user){
  //   this.appserviceProvider.checklogin(user).subscribe(
  //     data => {
  //       this.userDetails = data;
  //       if(this.userDetails === "Password Wrong") {
  //         this.base.presentToast(this.userDetails, 3500);
  //         console.error(this.userDetails);
  //         return;
  //       }
  //       if(this.userDetails === "UserName Not Valid") {
  //         this.base.presentToast(this.userDetails, 3500);
  //         console.error(this.userDetails);
  //         return;
  //       }
  //       localStorage.setItem('currentUser', JSON.stringify({
  //         token:this.userDetails.Token,
  //         name: this.userDetails.EmpName,
  //         EmpId: parseInt(this.userDetails.EmpId),
  //         subwareHouseId : this.userDetails.SubwareHouseId,
  //         role: this.userDetails.Role,
  //         teamId: this.userDetails.TeamId
  //       }));
  //       this.navCtrl.push(HomePage);
  //       const name = this.userDetails.EmpName;
  //       this.events.publish('user:created', name, Date.now());
  //     },
  //     err => {
  //      this.base.presentToast(err.error,3500);
  //       console.error(err);
  //     }
  //   );
  // }

  register() {
  }


  ParentforgetPassword() {
    debugger;
    this.navCtrl.navigateForward('parent-forgot-password');
  }
  ParentnewUser() {
    debugger;
    this.navCtrl.navigateForward('parent-signup');
  }
  EmployeeForgetPassword() {
    debugger;
    this.navCtrl.navigateForward('employee-forgot-password');
  }

}

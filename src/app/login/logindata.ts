export interface LoginData {
    Username: string;
    Password: string;
    OrgName: string;
}
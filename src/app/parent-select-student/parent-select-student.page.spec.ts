import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentSelectStudentPage } from './parent-select-student.page';

describe('ParentSelectStudentPage', () => {
  let component: ParentSelectStudentPage;
  let fixture: ComponentFixture<ParentSelectStudentPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentSelectStudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

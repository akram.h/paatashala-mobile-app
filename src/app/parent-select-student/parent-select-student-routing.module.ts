import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentSelectStudentPage } from './parent-select-student.page';

const routes: Routes = [
  {
    path: '',
    component: ParentSelectStudentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentSelectStudentPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import { ParentSelectStudentPageRoutingModule } from './parent-select-student-routing.module';

import { ParentSelectStudentPage } from './parent-select-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentSelectStudentPageRoutingModule,
    
  ],
  declarations: [ParentSelectStudentPage],

})
export class ParentSelectStudentPageModule {}

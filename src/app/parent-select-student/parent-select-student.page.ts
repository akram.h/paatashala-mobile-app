import { Component, OnInit } from '@angular/core';
import { NavController,NavParams } from '@ionic/angular';

@Component({
  selector: 'app-parent-select-student',
  templateUrl: './parent-select-student.page.html',
  styleUrls: ['./parent-select-student.page.scss'],
})
export class ParentSelectStudentPage implements OnInit {
  CurrentStudent:any;
  studentList:any;
  StIndex:any;
 constructor(public navCtrl: NavController, public navParams: NavParams) {
//this.studentList = JSON.parse(parentLoggedData).ParentStudentList;

this.CurrentStudent= this.studentList[0];
const currentStudentIndex = localStorage.getItem("CurrentStudentIndex");
  if (currentStudentIndex !== null) {
    this.StIndex = JSON.parse(currentStudentIndex);
  } else {
    // Handle the case where CurrentStudentIndex is not found in localStorage
    this.StIndex = null;
  }
debugger;
  }
  selectStudent(i :number)
  {
    debugger;
    this.CurrentStudent = this.studentList[i];
    localStorage.setItem('CurrentStudent',JSON.stringify(
{
  Name: this.CurrentStudent.Name,
  StudentId: this.CurrentStudent.Id,
  Batch: this.CurrentStudent.Batch,
  Course: this.CurrentStudent.Course,
  FatherName: this.CurrentStudent.FatherName,
  RegistrationCode: this.CurrentStudent.RegistrationCode,
  Sex: this.CurrentStudent.Sex ,
  OrgId: this.CurrentStudent.OrgId
}
    ));
    localStorage.setItem('CurrentStudentIndex', JSON.stringify(i));
    this.StIndex = JSON.parse(("CurrentStudentIndex"));

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentSelectStudentPage');
  }


  ngOnInit() {
  }

}

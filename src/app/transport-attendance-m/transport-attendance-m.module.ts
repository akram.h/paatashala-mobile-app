import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransportAttendanceMPageRoutingModule } from './transport-attendance-m-routing.module';

import { TransportAttendanceMPage } from './transport-attendance-m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransportAttendanceMPageRoutingModule
  ],
  declarations: [TransportAttendanceMPage]
})
export class TransportAttendanceMPageModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransportAttendanceMPage } from './transport-attendance-m.page';

describe('TransportAttendanceMPage', () => {
  let component: TransportAttendanceMPage;
  let fixture: ComponentFixture<TransportAttendanceMPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TransportAttendanceMPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

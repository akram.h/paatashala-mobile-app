import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';

@Component({
  selector: 'app-transport-attendance-m',
  templateUrl: './transport-attendance-m.page.html',
  styleUrls: ['./transport-attendance-m.page.scss'],
})
export class TransportAttendanceMPage implements OnInit {
  TrasportAttendanceObj: any;
  EmployeeLoggedData: any;
  BatchCourseObject: any;
  BatchList: any;
  CourseList: any;
  NextPageDiv: boolean;
  StudObject: any;
  StudentsList: any;
  SearchStudent: any;
  NextPage: any;
  AttendanceMarkAll: any;
  SubmittingAttendanceObject: any;
  ResultObject: any;

  constructor(public navCtrl: NavController, private appserviceProvider:AppserviceProvider) {
    this.TrasportAttendanceObj = {};
    this.EmployeeLoggedData = {};
    this.BatchCourseObject = {};
    this.StudObject = {};
    this.NextPage={};
    this.SubmittingAttendanceObject = {};
    this.ResultObject = {};
    this.BatchList = [];
    this.CourseList = [];
    this.StudentsList = [];
    this.SearchStudent = [];
    this.NextPageDiv = false;
    this.AttendanceMarkAll = [
      { Id: true, Name: 'Present'},
      { Id: false, Name: 'Absent'}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransportAttendanceMPage');
    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData")?.toString()??"");
    this.getBatchCourse();
  }
  getBatchCourse() {
    this.appserviceProvider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
      (  data: any) => {
    this.BatchCourseObject = data;
    this.BatchList = this.BatchCourseObject.Batches;
    this.CourseList = this.BatchCourseObject.Courses;
}
    );
  }
  getStudent() {
    debugger;
      this.SearchStudent = [];
      this.NextPageDiv = true;
      this.StudObject.CourseId = this.TrasportAttendanceObj.CourseId;
      this.StudObject.BatchId = this.TrasportAttendanceObj.BatchId;
      this.StudObject.OrgId = this.EmployeeLoggedData.OrgId;
      this.StudObject.AttendanceDate = this.TrasportAttendanceObj.AttendanceDate.toString();
      this.StudObject.Choice = this.TrasportAttendanceObj.Choice;
      this.appserviceProvider.getTransportStudentsBasedOnFiler(this.StudObject).subscribe(
        data => {
          this.StudentsList = data;
          this.SearchStudent = this.StudentsList;
          console.log(this.StudentsList);
          debugger;
        },
        err =>
        {
          debugger;
        }
      );
  }
  MarkAllStud() {
    debugger;
    this.StudentsList.forEach((element: { isPresent: any; }) => {
      element.isPresent = this.NextPage.Mark;
    });
    debugger;
  }
  onCancel(){}
  SearchStudentName(Event: any) {
    debugger;
    this.StudentsList = this.SearchStudent;
    const val = Event.target.value;
    if (val && val.trim() != '') {
      this.StudentsList = this.StudentsList.filter((item: { StudentName: string; }) => {
        return (item.StudentName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  SubmittingAttendance() {
    debugger;
    this.SubmittingAttendanceObject.DailyAttendanceObj = this.StudentsList;
    this.SubmittingAttendanceObject.dateAttendance = this.TrasportAttendanceObj.AttendanceDate.toString();
    this.SubmittingAttendanceObject.OrgId = this.EmployeeLoggedData.OrgId;
    this.SubmittingAttendanceObject.Choice = this.TrasportAttendanceObj.Choice;
    this.appserviceProvider.SubmittingAttendance(this.SubmittingAttendanceObject).subscribe(
      data => {
        debugger;
        this.ResultObject = data;
        if(this.ResultObject.status === true) {
          this.appserviceProvider.showAlert('Saved Successfully','');
        } else {
          this.appserviceProvider.showAlert('Somthing went Wrong','');
        }
      },
      err => {
        debugger;
      }
    );
  }

  ngOnInit() {
  }

}

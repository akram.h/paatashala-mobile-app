import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransportAttendanceMPage } from './transport-attendance-m.page';

const routes: Routes = [
  {
    path: '',
    component: TransportAttendanceMPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransportAttendanceMPageRoutingModule {}

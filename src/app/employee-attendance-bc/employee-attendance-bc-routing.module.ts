import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeAttendanceBcPage } from './employee-attendance-bc.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeeAttendanceBcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeAttendanceBcPageRoutingModule {}

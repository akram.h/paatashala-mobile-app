import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { IonicModule } from '@ionic/angular';

import { EmployeeAttendanceBcPageRoutingModule } from './employee-attendance-bc-routing.module';

import { EmployeeAttendanceBcPage } from './employee-attendance-bc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmployeeAttendanceBcPageRoutingModule
  ],
  declarations: [EmployeeAttendanceBcPage],
  providers: [AppserviceProvider,BarcodeScanner],
 
})
export class EmployeeAttendanceBcPageModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EmployeeAttendanceBcPage } from './employee-attendance-bc.page';

describe('EmployeeAttendanceBcPage', () => {
  let component: EmployeeAttendanceBcPage;
  let fixture: ComponentFixture<EmployeeAttendanceBcPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EmployeeAttendanceBcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

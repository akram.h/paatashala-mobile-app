import { Component, OnInit } from '@angular/core';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { NavController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-employee-attendance-bc',
  templateUrl: 'employee-attendance-bc.page.html',
  styleUrls: ['employee-attendance-bc.page.scss'],
})
export class EmployeeAttendanceBcPage implements OnInit {
  EmployeesList: any[];
  LoggedEmployeeData: any;
  scannedEmpolyeeList: any;
  Id: any;
  date: any;
  pick: any;
  empBCAttendanceData: any;
  result: any;
  constructor(public navCtrl: NavController,
    private appserviceProvider: AppserviceProvider,
    private barcodeScanner: BarcodeScanner
    )
   {
    this.EmployeesList = [];
    this.LoggedEmployeeData = JSON.parse(localStorage.getItem('EmployeeLoggedData') as any);

    this.scannedEmpolyeeList = {
      Id: [],
      Name: [],
      EmployeeId: []
    };
    this.date = new Date();
    this.empBCAttendanceData = {};
    this.pick = null;
    }
  
    ionViewDidEnter() {
      console.log('ionViewDidEnter EmployeeAttendanceBcPage');
      this.getEmployeesList();
    }
  
    ngOnInit() {
     
    }
  
    ScanBarCode() {
      this.barcodeScanner
        .scan()
        .then((barcodeData: any) => {
          this.Id = barcodeData.text;
          this.EmployeesList.forEach((element) => {
            if (element.EmpId == this.Id) {
              this.scannedEmpolyeeList.Name.push(element.Name);
              this.scannedEmpolyeeList.Id.push(element.Id);
              this.scannedEmpolyeeList.EmployeeId.push(element.EmpId);
            }
            console.log(this.scannedEmpolyeeList);
          });
        })
        .catch((err: any) => {
          console.log('Error', err);
        });
    }
  
  
  
    getEmployeesList() {
      this.appserviceProvider.getEmployeeslist(this.LoggedEmployeeData.OrgId).subscribe((data: any) => {
        this.EmployeesList = data; // Update to assign the data to EmployeesList
      });
    }
    
  
    DeleteCurrentRow(index: number) {
      this.scannedEmpolyeeList.Name.splice(index, 1);
      this.scannedEmpolyeeList.Id.splice(index, 1);
      this.scannedEmpolyeeList.EmployeeId.splice(index, 1);
    }
  
  SendEmployeeAtt(){
    this.empBCAttendanceData.OrgId = this.LoggedEmployeeData.OrgId;
    this.empBCAttendanceData.EmpId =  this.scannedEmpolyeeList.EmployeeId;
    this.empBCAttendanceData.scanDateTime = this.date.toString();
    this.empBCAttendanceData.IsCheckIn = this.pick;
  this.appserviceProvider.sendemployeeatt(this.empBCAttendanceData).subscribe(
  data=>{
    this.result=data;
    if(this.result.status==true){
      this.appserviceProvider.showAlert('Saved successfully ','');
    this.scannedEmpolyeeList = {
      Id: [],
      Name: [],
      EmployeeId: []
    }
  } else
  {
    this.appserviceProvider.showAlert('Failed',this.result.message);
  }
  }
  );
  }
  
  
  
  
 
}

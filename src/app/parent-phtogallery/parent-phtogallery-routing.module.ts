import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentPhtogalleryPage } from './parent-phtogallery.page';

const routes: Routes = [
  {
    path: '',
    component: ParentPhtogalleryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentPhtogalleryPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentPhtogalleryPageRoutingModule } from './parent-phtogallery-routing.module';

import { ParentPhtogalleryPage } from './parent-phtogallery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentPhtogalleryPageRoutingModule
  ],
  declarations: [ParentPhtogalleryPage]
})
export class ParentPhtogalleryPageModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentPhtogalleryPage } from './parent-phtogallery.page';

describe('ParentPhtogalleryPage', () => {
  let component: ParentPhtogalleryPage;
  let fixture: ComponentFixture<ParentPhtogalleryPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentPhtogalleryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { FileTransfer,FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Global } from 'src/shared/global';


declare const Element: any;
@Component({
  selector: 'app-parent-phtogallery',
  templateUrl: './parent-phtogallery.page.html',
  styleUrls: ['./parent-phtogallery.page.scss'],
})
export class ParentPhtogalleryPage implements OnInit {
  ImageList:any;
  studentData: any;
  ImageUrlList: any;
  baseURL: string = Global.BASE_API_URL;
  constructor(public navCtrl: NavController,private AppserviceProvider:AppserviceProvider) {
    this.studentData = JSON.parse(localStorage.getItem('CurrentStudent') || '{}');
    this.ImageUrlList=[];
    this.ImageList=[];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentPhtogalleryPage');
    this.getImgaes();
  }
  getImgaes()
  {
    debugger;
    this.AppserviceProvider.getParentGalleryImages(this.studentData).subscribe(
      data=>{
        debugger;
            this.ImageList = data;
            this.ImageList.forEach((element:any) => {
              const url = this.baseURL+"/Gallery/DownloadImage/"+element.Id;
              this.ImageUrlList.push(url);
              debugger;
            //  const img =  this.getImageDwnld(element.Id)
            //   const img = decodeURIComponent();

            });
      },
      err=>{

      }
    )
  }

  // DownloadImage(elemet:any)
  // {
  //   const fileTransfer: FileTransferObject = this.transfer.create();
  //   fileTransfer.download(elemet, "ttest.png").then((entry) => {
  //     console.log('download complete: ' + entry.toURL());  // I always enter here.
  //   }, (error) => {
  //     // handle error
  //     console.log("error!");
  //   });
  // }

  getImageDwnld(element: any) {
    if (element) {
      // Handle when 'element' is defined...
      //this.photoViewer.show(element, 'Gallery', { share: true });
    } else {
      // Handle when 'element' is undefined...
    }
  }


  ngOnInit() {
  }

}

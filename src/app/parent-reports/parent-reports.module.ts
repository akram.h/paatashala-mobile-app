import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentReportsPageRoutingModule } from './parent-reports-routing.module';

import { ParentReportsPage } from './parent-reports.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentReportsPageRoutingModule
  ],
  declarations: [ParentReportsPage]
})
export class ParentReportsPageModule {}

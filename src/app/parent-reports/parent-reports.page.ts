import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { Global
 } from 'src/shared/global';

@Component({
  selector: 'app-parent-reports',
  templateUrl: './parent-reports.page.html',
  styleUrls: ['./parent-reports.page.scss'],
})
export class ParentReportsPage implements OnInit {
  groupedReport: any;
  SelectedStudentData: any;
  baseURL: string = Global.BASE_API_URL;
    constructor(public appservice: AppserviceProvider,public navCtrl: NavController ) {
      this.groupedReport = [];
      this.SelectedStudentData = {};
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad ParentReportsPage');

     const storedData = localStorage.getItem("CurrentStudent");
if (storedData !== null) {
  this.SelectedStudentData = JSON.parse(storedData);
} else {
  // Handle the case when the data is not available in localStorage.
}
  this.getReportBaseDetails();
    }
  
    getReportBaseDetails() {
      debugger;
      this.appservice.getReportBaseDetails(this.SelectedStudentData.StudentId,this.SelectedStudentData.OrgId).subscribe(
      data => {
        debugger;
        this.groupedReport = data;
        this.groupedReport.forEach((element: Report) => {
          element.pdfUrl = this.baseURL + 'Report/getReportEntireDetails?Id=' + element.id + '&OrgId=' + this.SelectedStudentData.OrgId;
        });
      debugger;
    },
    err => {
      console.log(err);
    }
  );
    }
  

  ngOnInit() {
  }

}
export interface Report {
  id: number;
  title: string;
  pdfUrl: string; // Add the pdfUrl property of type string
  // ... other properties
}

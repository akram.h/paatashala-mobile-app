import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentReportsPage } from './parent-reports.page';

describe('ParentReportsPage', () => {
  let component: ParentReportsPage;
  let fixture: ComponentFixture<ParentReportsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentReportsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

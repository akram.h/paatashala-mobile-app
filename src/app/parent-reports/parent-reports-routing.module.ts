import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentReportsPage } from './parent-reports.page';

const routes: Routes = [
  {
    path: '',
    component: ParentReportsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentReportsPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MedicineReportPageRoutingModule } from './medicine-report-routing.module';

import { MedicineReportPage } from './medicine-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MedicineReportPageRoutingModule
  ],
  declarations: [MedicineReportPage]
})
export class MedicineReportPageModule {}

import { AppserviceProvider } from 'src/providers/appservice/appservice';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-medicine-report',
  templateUrl: './medicine-report.page.html',
  styleUrls: ['./medicine-report.page.scss'],
})
export class MedicineReportPage implements OnInit {
  MedicineTab: string = '';
  medicineObject:any;
  EmployeeLoggedData:any;
  Result: any;
  BatchCourseObject: any;
  BatchList: any;
  CourseList: any;
  BatchStudent:any;
  studentList: any;
  studformBool: any;
  medFormBool:any;
  medicineList:any;
  StudentMed:any;
  constructor(public navCtrl: NavController,private appserviceProvider:AppserviceProvider,private activatedRoute: ActivatedRoute) {
    this.medicineObject = {};
    this.Result = {};
    this.BatchCourseObject = {};
    this.BatchList = [];
    this.CourseList = [];
    this.BatchStudent={};
    this.studentList=[];
    this.studformBool = true;
    this.medFormBool = false;
    this.medicineList = [];
    this.StudentMed ={};
    this.EmployeeLoggedData = JSON.parse(localStorage.getItem("EmployeeLoggedData") ?? '{}');
  }
ngOnInit() {
  
  this.MedicineTab = "MedicineType";
   this.getBatchCourse();
   this.medList();

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicineReportPage');

  }

  getBatchCourse() {

    this.appserviceProvider.getBatchCourse(this.EmployeeLoggedData.OrgId).subscribe(
  data => {
    this.BatchCourseObject = data;
    this.BatchList = this.BatchCourseObject.Batches;
    this.CourseList = this.BatchCourseObject.Courses;
    //debugger;
}
    );
  }

  getStudentList(){

 this.BatchStudent.OrgId = this.EmployeeLoggedData.OrgId;
 this.appserviceProvider.getStudentList(this.BatchStudent).subscribe(
  data=>{
    console.log(data);
    this.studentList=data;
  }

);

  }

  OnSubmit(){
    //debugger;
this.medicineObject.OrgId = this.EmployeeLoggedData.OrgId;
this.appserviceProvider.AddMedicine(this.medicineObject).subscribe(
  data=>{
    this.Result = data;
    this.medicineObject = {
      Name: '',
      Description: '',
      Company: '',
      OrgId: this.EmployeeLoggedData.OrgId
    };
    if(this.Result.status == true) {
    this.appserviceProvider.showAlert('Saved Successfully','');
    console.log(this.medicineObject);
    //debugger;
    }
    else
    this.appserviceProvider.showAlert('Failed', this.Result.message);
  }
);
  }
  Change() {
this.studformBool = false;
this.medFormBool = true;
this.medList();
this.StudentMed.Date = new Date().toISOString();
this.StudentMed.Time = new Date().toISOString();
  }
afterChange(){
  this.studformBool = true;
this.medFormBool = false;
}
medList(){
this.appserviceProvider.getMedList( this.EmployeeLoggedData.OrgId).subscribe(
      data=>{
        console.log(data);
        this.medicineList=data;
      }
    );
  }

  SaveStudMed(){
    this.StudentMed.StudentId = this.BatchStudent.StudentId;
    this.StudentMed.OrgId = this.EmployeeLoggedData.OrgId;
    this.appserviceProvider.saveStudMed(this.StudentMed).subscribe(
      data=>{
        this.Result = data;
        if(this.Result.status == true) {
          this.appserviceProvider.showAlert('Saved Successfully','');
          this.afterChange();
          this.BatchStudent ={
            BatchId:'',
            CourseId:'',
            Date:'',
            StudentId:'',
            OrgId: this.EmployeeLoggedData.OrgId
          };
          this.StudentMed = {
            MedicineId :'',
            Dosage :'',
            Date :'',
            StudentId :'',
            Time:'',
            OrgId: this.EmployeeLoggedData.OrgId
          };
        } else
        this.appserviceProvider.showAlert('Failed', this.Result.message);
      }
    );
  }


}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicineReportPage } from './medicine-report.page';

const routes: Routes = [
  {
    path: '',
    component: MedicineReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicineReportPageRoutingModule {}

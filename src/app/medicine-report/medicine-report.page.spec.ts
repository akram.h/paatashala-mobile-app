import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MedicineReportPage } from './medicine-report.page';

describe('MedicineReportPage', () => {
  let component: MedicineReportPage;
  let fixture: ComponentFixture<MedicineReportPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MedicineReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

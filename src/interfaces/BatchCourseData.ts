export interface Batch {
    Id: number;
    Name: string;
  }
  
  // Data structure for Course
  export interface Course {
    Id: number;
    Name: string;
  }
  
  export interface BatchCourseData {
    Batches: Batch[];
    Courses: Course[];
  }
import { Injectable } from "@angular/core";
import { AlertController, LoadingController, LoadingOptions, ToastController } from "@ionic/angular";

@Injectable()
export class Base{ 
    globalArray: any = [];
    timer: number | undefined;
    notificationCount: number = 0;
    notification: any;
    toast: any;
    loading: any;

  constructor(public toastCtrl: ToastController, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    this.showConsole("");
  }

  async presentAlert(title: string) {
    let alert = await this.alertCtrl.create({
      header: title,
      buttons: ['OK']
    });
    alert.present();
  }

  async getLoadingPopUp() {
    let configPopup: LoadingOptions = {
      spinner: null,
      message: `
      <div class="custom-spinner-container">
      <div class="custom-spinner-box"></div>
    </div>`
    };
    
    return await this.loadingCtrl.create(configPopup);
  }

  showLoadingPopUp() {
    this.loading = this.getLoadingPopUp();
    this.loading.present();
  }

  hideLoadingPopUp() {
    try {
      this.loading.dismiss();
    } catch (e) {
      this.showConsole(e);
    }

  }
  //for shownig console
  showConsole(dataV: any) {
    console.log(dataV);
  }

  //For setting localstorage
  setLocalStorage(key: string, value: string) {
    localStorage.setItem(key, value);
  }
  //For settin getlocalstorage
  getLocalStorage(key: string) {
    return localStorage.getItem(key);
  }
  //Clear LocalStorage
  clearLocalStorage() { 
    debugger;
    localStorage.clear();    
  }

loadingNormal(){
  this.loading = this.loadingCtrl.create({
    message: 'Please wait...'
  });

  this.loading.present();

  setTimeout(() => {
    this.loading.dismiss();
  }, 5000);
}

  showToast(message: any, ok = false, duration = 2000) {
    if (this.toast) {
      this.toast.dismiss();
    }
    this.toast = this.toastCtrl.create({
      message,
      duration: ok ? 0 : duration,
      position: 'bottom'
    });
    this.toast.present();
  }

  presentToast(msg: string,time: number) {
    this.toast = this.toastCtrl.create({
      message: msg,
      duration: time,
      position: 'top'
    });
  
    this.toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    this.toast.present();
  }

  async confirmAlert(){
    let alert = await this.alertCtrl.create({
      header: 'Confirm purchase',
      message: 'Do you want to Proceed?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            return false;
          }
        },
        {
          text: 'Buy',
          handler: () => {
            return true;
          }
        }
      ]
    });
     alert.present();
  }

  async clickDownloadAlert(){
    let alert = await this.alertCtrl.create({
      header: 'Update Avalidable',
      message: '<a href="http://54.251.177.132:4500/file/downloads">Click Here </a>',
    });
    alert.present();
  }


}
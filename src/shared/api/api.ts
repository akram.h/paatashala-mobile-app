import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Global } from "../global";
import { Base } from "../base/base";

@Injectable()
export class Api {
  url: string = Global.BASE_API_URL;
  Userdetails:any;
  EmployeeLoggedData: any;
  UserType: any;
  constructor(public http: HttpClient, public base: Base) {
   }


  getToken()
  {
    //debugger;
    if(this.UserType=="Employee")
    {
      this.Userdetails = JSON.parse(localStorage.getItem("EmployeeLoggedData")?.toString()??"");
    }
    else
    {
      this.Userdetails = JSON.parse(localStorage.getItem("ParentLoggedData")?.toString()??"");
    }
    return this.Userdetails.token;
  }

  get(relativeUrl: string) {

    //debugger;
    //debugger;
    const authToken = this.getToken();
    const theaders = new HttpHeaders().set('Token', authToken);
    let url = this.url + relativeUrl;
    return this.http.get(url,{headers:theaders});
  }

  post(relativeUrl: string, resource:any) {
    //debugger;
    const authToken = this.getToken();
    const theaders = new HttpHeaders().set('Token', authToken);
    let url = this.url + relativeUrl;
    return this.http.post(url, resource,{headers:theaders});
  }

  setUserType(type: string)
  {
    this.UserType=type;
  }



  // getEmployee(relativeUrl: string,resource) {
  //   this.Userdetails = JSON.parse(localStorage.getItem('EmployeeLoggedData'));
  //   let url = this.url +relativeUrl + "?OrgId="+resource;
  //    const authToken =  this.Userdetails.token;
  //    const theaders = new HttpHeaders().set('Token', authToken);
  //   debugger;
  //   return this.http.get(url,{headers:theaders});
  // }


  // getHeaders() {
  //   let options = new RequestOptions();
  //   options.headers = new Headers({ 'Content-Type': 'application/json' });
  //   options.headers.append('Authorization', 'Bearer ' + this.base.getLocalStorage('Token'));
  //   return options;
  // }
  // getHeaderOptions() {
  //   debugger;
  //   this.Userdetails = JSON.parse(localStorage.getItem('EmployeeLoggedData'));
  //   if ((this.Userdetails == undefined || this.Userdetails == null) ) {
  //   //  this.router.navigate(['login/']);
  //   }
  //   else {
  //     const authToken = 'Bearer ' + this.Userdetails.token;
  //     const headers = new HttpHeaders().set('Authorization', authToken);
  //     return headers;
  //   }
  // }

}

